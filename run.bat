@echo off

echo Building...
call build.bat
if %errorlevel% neq 0 exit /b %errorlevel%

setlocal

set SDK_ROOT=n:\env
set CSPECT=%SDK_ROOT%\cspect\cspect.exe
set MONKEY=%SDK_ROOT%\hdfmonkey\hdfmonkey

echo Running...
%MONKEY% put %SDK_ROOT%/tbblue.mmc etc/autoexec.bas /nextzxos
%CSPECT% -basickeys -r -tv -brk -16bit -s28 -w3 -zxnext -map=odin.map -mmc=%SDK_ROOT%/tbblue.mmc

