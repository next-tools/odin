rm -rf dist
mkdir -p dist/odin/files
cp odin dist/odin/files
cp docs/odin.gde dist/odin/files
cp docs/odin-editor.gde dist/odin/files
cp docs/odin-monitor.gde dist/odin/files
cp docs/odin-debugger.gde dist/odin/files
cp etc/install.bas dist/odin

# Copy data
mkdir -p dist/odin/data
cp data/topan.fnt dist/odin/data
cp data/font-license.txt dist/odin/data

# Copy libraries
mkdir -p dist/odin/lib
cp lib/*.odn dist/odin/lib

# Zip it
cd dist
zip -r9 odin.zip odin
cd -
cp dist/odin.zip .
rm -rf dist
