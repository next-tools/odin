;;----------------------------------------------------------------------------------------------------------------------
;; Command handlers
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Command tables

CmdNameTable:
                dz      "H"             ; Help
                dz      "S"             ; Save
                dz      "SA"            ; Save All
                dz      "L"             ; Load
                dz      "A"             ; Assemble
                dz      "T"             ; Show symbol table
                dz      "R"             ; Run code
                dz      "NEW"           ; New document
                dz      "TL"            ; Text load
                dz      "TS"            ; Text save
                dz      "INFO"          ; Memory information
                dz      "LS"            ; List files in current directory
                dz      "CD"            ; Change directory
                dz      "MD"            ; Make directory
                dz      "M"             ; Memory dump
                dz      "REMOUNT"       ; Remount SD card
                db      0

;;----------------------------------------------------------------------------------------------------------------------
;; This command table lists the parameter formats for each command. The format of each entry is:
;;
;;      Offset  Length  Description
;;      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;;      0       2       Address of handler
;;      2       3       Number of parameters
;;
;; Then for each parameter:
;;
;;      Offset  Length  Description
;;      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;;      0       1       Parameter type: (s)tring, (n)umber
;;      1       4       Default value if not provided
;;

CmdTable:
                dw      cmdHelp
                dw      cmdSave
                dw      cmdSaveAll
                dw      cmdLoad
                dw      cmdAssemble
                dw      cmdSymTable
                dw      cmdExec
                dw      cmdNew
                dw      cmdTextLoad
                dw      cmdTextSave
                dw      cmdMemInfo
                dw      cmdLS
                dw      cmdCD
                dw      cmdMD
                dw      cmdDump
                dw      cmdRemount

;;----------------------------------------------------------------------------------------------------------------------
;; cmdHelp

cmdHelp:
                call    print
                db      C_CLEARLINE,"Commands:",C_ENTER
                db      C_CLEARLINE,"  A                        Assemble current source",C_ENTER
                db      C_CLEARLINE,"  L <filename>             Load in a document",C_ENTER
                db      C_CLEARLINE,"  NEW <filename>           Create new document",C_ENTER
                db      C_CLEARLINE,"  S <filename>             Save out a document",C_ENTER
                db      C_CLEARLINE,"  SA                       Save all unsaved named files",C_ENTER
                db      C_CLEARLINE,"  T                        Show symbols from last assemble",C_ENTER
                db      C_CLEARLINE,"  TL <filename>            Load in a text-based document",C_ENTER
                db      C_CLEARLINE,"  TS <filename>            Save out a text-based document",C_ENTER
                db      C_CLEARLINE,"  R                        Execute assembly and enter debugger",C_ENTER
                db      C_CLEARLINE,"  INFO                     Memory information",C_ENTER
                db      C_CLEARLINE,"  LS                       Show files in directory",C_ENTER
                db      C_CLEARLINE,"  CD                       Change or show current directory",C_ENTER
                db      C_CLEARLINE,"  MD                       Create a directory",C_ENTER
                db      C_CLEARLINE,"  M                        Memory dump",C_ENTER
                db      C_CLEARLINE,"  REMOUNT                  Remount all drives",C_ENTER
                db      C_CLEARLINE,C_ENTER
                db      C_CLEARLINE,"Hotkeys (Monitor):",C_ENTER
                db      C_CLEARLINE,"  Ext+Q                    Quit Odin (if not used OPT NEX)",C_ENTER
                db      C_CLEARLINE,"  Ext+L                    Clear screen",C_ENTER
                db      C_CLEARLINE,"  Ext+O                    Overwrite mode",C_ENTER
                db      C_CLEARLINE,"  Ext+E                    Clear to end of line",C_ENTER
                db      C_CLEARLINE,"  Ext+X                    Clear whole line",C_ENTER
                db      C_CLEARLINE,"  Ext+Cursor Left          Beginning of line",C_ENTER
                db      C_CLEARLINE,"  Ext+Cursor Right         End of line",C_ENTER
                db      C_CLEARLINE,"  Ext+Delete               Delete forward",C_ENTER
                db      C_CLEARLINE,C_ENTER,0
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdHandle
;; Fetch the parameters in the input, validate them and call the handler
;;
;; Input:
;;      HL = Input buffer
;;      A = command letter

cmdHandle:
                ; Find the command in the table
                ex      de,hl
                ld      hl,CmdNameTable
                ld      b,0
                push    de
.l1:
                ; Reached end of current command?
                ld      a,(hl)
                and     a
                jr      z,.end_cmd              ; End of command found, check input buffer to see if it ended.

                ld      a,(de)                  ; Get command
                call    upperCase
                cp      (hl)
                jr      nz,.next_cmd

                inc     hl
                inc     de
                jr      .l1
.next_cmd:
                ldi     a,(hl)
                and     a
                jr      nz,.next_cmd
                inc     b
                pop     de
                push    de
                ld      a,(hl)
                and     a                       ; Found end of command list?
                jr      z,.unknown
                jr      .l1
.end_cmd:
                ; Here the input has matched a command so far.  The input may still have extra characters.
                ld      a,(de)
                call    isAlpha
                jr      c,.found_cmd            ; Not a letter: so end of command in input buffer too
                jr      .next_cmd

.found_cmd:
                pop     hl      ; Discard the input buffer pointer

                ; DE = parameters
                push    bc
                di                      ; Have to disable interrupts because lex uses the keyboard buffer
                ld      a,(AsmCode)
                ld      hl,lex
                call    farCall         ; AsmBuffer contains the parameters

                pop     bc
                ld      hl,CmdTable
                ld      a,b
                add     a,a
                add     hl,a
                ldhl                    ; HL = Command handler, DE = param tokens
                ld      de,AsmBuffer
                jp      (hl)

.unknown:
                pop     de
                call    errorPrint
                dz      "UNKNOWN COMMAND"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; setFullName
;; Set the full name of a document
;;
;; Input:
;;      HL = start of string
;;      BC = end of string
;;
;; Output:
;;      Full filename in FileName
;;      CF = 1 if error occurs
;;

setFullName:
        push    de
        ld      de,bc
        call    fNormalise
        pop     de
        ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdExpectEnd
;; Expect a null terminator in the parameters
;;
;; Input:
;;      DE = tokens buffer
;; 
;; Output:
;;      CF = 1 if no null terminator in input
;;      DE is unchanged
;;

cmdExpectEnd:
                ld      a,(de)
                and     a
                ret     z
                call    errorPrint
                dz      "TOO MANY PARAMETERS"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdExpectString
;; Expect a string in the parameters
;;
;; Input:
;;      DE = tokens buffer (256-byte aligned buffer)
;;
;; Output:
;;      CF = 1 if no string found
;;      HL = Start of string
;;      BC = End of string
;;      DE = pointer after string tokens
;;

cmdExpectString:
                ld      a,(de)
                cp      T_STRING
                jr      nz,.not_string
.extract:
                inc     e
                push    de
                ex      de,hl           ; HL = start of string
                call    strEnd          ; HL = end of string pointing at null terminator
                ld      bc,hl
                ex      de,hl           ; DE = end of string
                inc     e
                pop     hl
                and     a
                ret

.not_string:
                cp      T_SYMBOL
                jr      z,.extract

                call    errorPrint
                dz      "STRING PARAMETER EXPECTED"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdExpectFileName
;; Expect a single filename argument and put the full filename into the buffer FileName :)
;;
;; Input:
;;      DE = tokens buffer (256-byte aligned buffer)
;;
;; Output:
;;      CF = 1 if error occurs
;;
;; Affects:
;;      HL,BC,DE
;;

cmdExpectFileName:
                call    cmdExpectString
                ret     c
                call    cmdExpectEnd
                ret     c
                call    setFullName
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdExpectAddress
;; Expect an address, which currently will be an integer
;;
;; Input:
;;      DE = tokens buffer (256-byte aligned buffer)
;;
;; Output:
;;      CF = 1 if not an integer
;;      HL = 16-bit integer
;;

cmdExpectAddress:
                ld      a,(de)
                cp      T_VALUE
                jr      nz,.not_addr

                inc     e
                ex      de,hl
                ld      e,(hl)
                inc     l
                ld      d,(hl)
                inc     l
                ex      de,hl
                and     a
                ret
.not_addr:
                cp      T_SYMBOL
                jr      nz,.not_sym

                ld      a,(AsmCode)
                ld      hl,symLookUp
                inc     e
                call    farCall
                jr      c,.unknown
                ld      hl,bc
                ret
.unknown:
                call    errorPrint
                dz      "UNKNOWN SYMBOL"
                ret

.not_sym:
                call    errorPrint
                dz      "ADDRESS PARAMETER EXPECTED"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdSaveDoc
;; Save a document of a particular index.  It does this by swapping the required document into slot 0 (main document)
;; and calling the usual save routines (which only save main document), then swaps them back.
;;
;; Input:
;;      A = document index
;;
;; Only BC is restored
;;

cmdSaveDoc:
                and     a
                jp      z,.save

                push    bc

                ; Swap document we want to save into current slot
                ld      b,a
                ld      c,0
                call    docSwap

                call    .save

                ; Swap the document back
                push    af
                call    docSwap
                pop     af
                pop     bc
                ret
.save:
                push    bc
                ld      a,(MainDoc.Flags)
                bit     0,a
                jr      z,.not_dirty
                call    cmdSave.save
.not_dirty:
                pop     bc
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; cmdSaveAll
;; Save all files in memory
;;

cmdSaveAll:
                call    cmdExpectEnd
                ret     c

.ignore_params:
                ld      b,0
.l0:
                ld      a,b
                call    docInfo
                ld      a,(hl)
                inc     hl
                or      (hl)
                ret     z               ; No more documents!

                ld      a,b
                call    cmdSaveDoc      ; Attempt to save it
                ret     c
                inc     b
                ld      a,b
                cp      kNumDocs+1
                jr      c,.l0
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdSave
;; Save the current document
;;
;; Output:
;;      CF = 1 if error occurs
;;
;; Args:
;;      <filename> (default is current filename)
;;

cmdSave:
                call    checkForDoc
                ret     c

                ld      a,(de)
                and     a
                jr      z,.save                 ; Save with current filename

                ; Set up the filename from the parameters
                call    cmdExpectFileName
                ret     c
                call    fEnsureOdn
                ret     c
                ld      hl,FileName
                ld      de,MainDocName
                call    strcpy

.save:
                ; Check to see if we have a valid name
                ld      a,(MainDocName)
                and     a
                jr      z,.no_name

                call    docSave
                jr      c,.bad_save

                ld      a,kInkSuccess
                call    colouredPrint
                dz      C_CLEARLINE,"Document saved: "
                ld      hl,MainDocName
                call    printHL
                call    print
                dz      C_ENTER
                and     a

                ret

.bad_save:
                ld      a,kInkError
                call    colouredPrint
                dz      "UNABLE TO CREATE FILE: "
                ld      hl,MainDocName
                call    printHL
                call    print
                dz      C_ENTER
                scf
                ret

.no_name:
                call    errorPrint
                dz      "DOCUMENT WITH NO NAME FOUND"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdLoad
;; Load a document
;;
;; Args:
;;      <filename>
;;

cmdLoad:
                ; Check we have a name
                call    cmdExpectFileName
                ret     c
                call    fEnsureOdn
                ret     c

                ; See if it's loaded already, and if so, switch to it.
                ld      hl,FileName
                call    docFind
                jr      c,.not_found

                ld      b,a
                ld      c,0             ; BC = ready for docSwap call

                ld      a,kInkSuccess
                call    colouredPrint
                dz      C_CLEARLINE,"Documented already in memory",C_ENTER

                jp      docSwap

.not_found:
                call    docLoad
                jr      c,.error

                ; Show success
                ld      a,kInkSuccess
                call    colouredPrint
                dz      C_CLEARLINE,"Document loaded",C_ENTER
                ret

.error:
                call    errorPrint
                dz      "FILE NOT FOUND"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; checkForDoc
;; Will return CF=1 and show an error message if there is no document.
;;

checkForDoc:
                push    bc,de,hl
                push    af
                call    docExists
                jr      nc,.ok

                pop     af
                call    errorPrint
                dz      "NO DOCUMENT FOUND"
                jr      .done
.ok:
                pop     af
                and     a
.done:
                pop     hl,de,bc
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; cmdAssemble
;;
;; Args:
;;      None
;;

FNameAsm        equ     FNamesUserPage+0

cmdAssemble:
                call    checkForDoc
                ret     c

                ; Check to see if we have a parameter
                ld      a,(de)
                and     a
                jr      nz,.have_param          ; Yes, extract it to FileName and check

                ; No, so check we already have a previous name before
                ld      a,FNameAsm
                call    docName
                ld      a,(hl)
                and     a
                jr      nz,.have_name

                ; No, so transfer current document name into Filename buffer and check
                ld      hl,MainDocName
                ld      a,(hl)
                and     a
                jr      z,.no_named_doc

                ld      de,FileName
                ld      bc,256
                call    memcpy
                jr      .check_doc
.no_named_doc:
                call    errorPrint
                dz      "CURRENT DOCUMENT HAS NO NAME"
                ret

.have_name:
                ; Find the document given by previous assembler doc name
                call    docFind
                jp      nc,.asm

.doc_error:
                call    errorPrint
                dz      "MAIN DOCUMENT IS NOT OPENED"
                ret

.have_param:
                call    cmdExpectFileName
                ret     c
                call    fEnsureOdn
                ret     c

.check_doc:
                ld      hl,FileName
                call    docFind
                jr      c,.doc_error            ; TODO: Allow main document to be unloaded

                push    af
                ex      de,hl
                ld      a,FNameAsm
                call    docName
                ex      de,hl                   ; HL = filename, DE = assembler doc name
                ld      bc,256
                call    memcpy

.found_doc:
                ; Output a message to the user that we have changed state
                call    pageVideo
                ld      a,kInkComment
                call    setColour
                call    print
                dz      C_CLEARLINE,"Switching assembler main doc to:",C_ENTER
                ld      hl,FileName
                call    printHL
                ld      a,C_ENTER
                call    printChar
                ld      a,kInkNormal
                call    setColour

                pop     af                      ; Restore document index

.asm:
                ; A = document to assemble
                ; Save all the documents first
                push    af
                call    cmdSaveAll.ignore_params
                jr      c,.no_save

                ; Clear the clipboard
                ld      a,(EditorCode)
                ld      hl,clipClear
                call    farCall

                ld      hl,asmMain
                ld      a,(AsmCode)
                pop     bc                      ; B = document index
                call    farCall
                jr      c,.error
                ld      a,kInkSuccess
                call    colouredPrint
                dz      C_CLEARLINE,"ASSEMBLED OK",C_ENTER
                ret
.error:
                ; HL = current line
                ret
.no_save:
                pop     af
                call    errorPrint
                dz      "UNABLE TO SAVE DOCUMENTS BEFORE ASSEMBLING"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdSymbolTable
;;
;; Args:
;;      <prefix> (optional)
;;

cmdSymTable:
                ld      hl,de
                ld      a,(hl)
                and     a
                jr      z,.no_param

                call    cmdExpectString
                ret     c
                call    cmdExpectEnd
                ret     c

.no_param
                call    resetLineCount          ; Enables interrupts and keyboard
                ld      (.match_word),hl

                call    pageVideo
                call    cursorHide

                xor     a
                ld      (LineCount),a
                ld      a,kInkResponse
                call    setColour
                ld      bc,(NumSyms)
                ld      a,b
                or      c
                jr      z,.no_syms
                ld      ix,SymPages
.next_page:
                ld      a,(ix+0)
                inc     ix
                page    4,a             ; Page in symbol table page
                ld      hl,addrMMU4
.next_sym:
                ; Check to see if current symbol has required prefix
                push    hl
                inc     l
                ld      de,(.match_word)
.l0:
                ld      a,(de)
                and     a
                jr      z,.show_sym
                cp      (hl)
                jr      nz,.skip_sym
                inc     de
                inc     l
                jr      .l0
.skip_sym:
                pop     hl
                jr      .next

.show_sym:
                pop     hl
                push    hl
                ld      a,SymInfo.Value
                add     hl,a
                ldhl                    ; HL = value

                ; Print value
                ld      a,C_CLEARLINE
                call    printChar
                ld      a,'$'
                call    printChar
                call    printHexWord
                ld      a,':'
                call    printChar
                call    printSpace

                ; Print symbol
                pop     hl
                push    hl
                inc     l
                call    printHL
                ld      a,C_ENTER
                call    printChar

                push    bc
                call    checkLineCount
                pop     bc
                pop     hl
                jr      z,.done

                ; Next iteration
.next:
                ld      a,32
                add     hl,a
                dec     bc
                ld      a,b
                or      c
                jr      z,.done
                ld      a,h
                cp      $a0
                jr      c,.next_sym

                ld      h,$80           ; Reset HL to beginning of page
                jr      .next_page

.no_syms:
                call    cursorShow
                call    print
                dz      C_CLEARLINE, "No symbols", C_ENTER
                call    cursorHide
.done:
                ld      a,kInkNormal
                call    setColour
                call    cursorShow
                ret

.match_word     dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; cmdExec
;;
;; Args:
;;      None
;;

cmdExec:
                call    cmdExpectEnd
                ret     c
                
                call    pageDebugger

                ; Set up return context
                rreg    $56
                ld      (OdinMMU6),a
                rreg2   $57
                ld      (OdinMMU7),a

                ; Set up initial MMU state
                ld      hl,MMUState+1
                ld      de,DebuggerMMU
                ld      bc,7
                ldir

                ; Set up initial PC
                ld      hl,(StartAddress)
                ld      (ProgStart),hl

                ; Turn off console mode
                di
                call    cursorOff
                call    cursorHide
                call    modeULA

                ; Call debugger
                rst     0

                ; Restore console mode
                call    modeConsole
                call    cursorShow
                call    cursorOn
                ei
                halt
                call    flushKeys

                ; Output the registers
                call    cursorHide
                ld      hl,RegOutput
.l1:
                ldi     a,(hl)
                and     a
                jr      z,.end

                cp      $0e
                jr      nz,.not_num8
                ldi     e,(hl)
                ldi     d,(hl)
                ld      a,(de)
                call    printHexByte
                jr      .l1
.not_num8:
                cp      $0f
                jr      nz,.not_num16
                ldi     e,(hl)
                ldi     d,(hl)
                ex      de,hl
                ldhl
                call    printHexWord
                ex      de,hl
                jr      .l1
.not_num16:
                call    printChar
                jr      .l1

.end:
                ; Output the flags

                call    print
                dz      C_CLEARLINE,"Flags: "
                ld      a,(RegAF)       ; rdlow-ok
                ld      c,a
                call    .show_flags

                call    print
                dz      " ("
                ld      a,(RegAFA)      ; rdlow-ok
                ld      c,a
                call    .show_flags
                call    print
                dz      ")",C_ENTER,C_CLEARLINE,C_ENTER

                call    print
                dz      "HL:"
                ld      hl,(RegHL)      ; rdlow-ok
                call    dumpReg

                call    print
                dz      "DE:"
                ld      hl,(RegDE)      ; rdlow-ok
                call    dumpReg

                call    print
                dz      "IX:"
                ld      hl,(RegIX)      ; rdlow-ok
                call    dumpReg

                call    print
                dz      "IY:"
                ld      hl,(RegIY)      ; rdlow-ok
                call    dumpReg

                ld      a,C_ENTER
                call    printChar

                jp      cursorShow

.show_flags
                ld      b,8
                ld      hl,FlagNames
.flag_loop:
                rlc     c
                jr      c,.set
                ld      a,kInkFlagOff
                jr      .print_flag
.set            ld      a,kInkFlagOn
.print_flag     call    setColour
                ldi      a,(hl)
                call    printChar
                djnz    .flag_loop

                ld      a,kInkNormal
                jp      setColour

FlagNames       db      "SZ-H-VNC"

dumpReg:
                ; HL=address
                call    fillMonitorBuffer
                ld      hl,MonitorBuffer
                jp      dumpLine

RegOutput:
                db      C_CLEARLINE,C_ENTER

                db      C_CLEARLINE,"AF: ",$0f
                dw      RegAF
                db      " ",$0f
                dw      RegAFA
                db      "   IX: ",$0f
                dw      RegIX
                db      C_ENTER

                db      C_CLEARLINE,"BC: ",$0f
                dw      RegBC
                db      " ",$0f
                dw      RegBCA
                db      "   IY: ",$0f
                dw      RegIY
                db      C_ENTER
                
                db      C_CLEARLINE,"DE: ",$0f
                dw      RegDE
                db      " ",$0f
                dw      RegDEA
                db      "   SP: ",$0f
                dw      RegSP
                db      C_ENTER
                
                db      C_CLEARLINE,"HL: ",$0f
                dw      RegHL
                db      " ",$0f
                dw      RegHLA
                db      "    I: ",$0e
                dw      RegI
                db      C_ENTER

                db      C_CLEARLINE,"PC: ",$0f
                dw      RegPC
                db      C_ENTER
                
                dz      C_CLEARLINE,C_ENTER

;;----------------------------------------------------------------------------------------------------------------------
;; cmdNew
;; Clear the document
;;
;; Args:
;;      <filename> - (default: no name)
;;

cmdNew:
                ; Do we have a filename?
                ld      a,(de)
                and     a
                jp      z,docNewNoName
                call    cmdExpectFileName
                ret     c
                call    fEnsureOdn
                ret     c
                ld      hl,FileName
                call    docFind
                jr      nc,.already_exists

                ;;TODO: incorporate docFind to prevent opening second file with same name
                jp      docNew
.already_exists:
                call    errorPrint
                dz      "ERROR: File already exists in memory"
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; cmdTextLoad
;; Load a text document, and tokenise it into memory
;;
;;
;; Args:
;;      <filename>
;;

cmdTextLoad:
                call    cmdExpectFileName       ; sets up FileName for `docNew`
                ret     c

                call    fOpenLoad
                jp      c,cmdLoad.error
                push    hl                      ; Save the file buffer

                ;TODO patch/append FileName extension to .odn

                call    docNew
                pop     hl
                jr      c,.oom

                ; HL = source buffer read from file, new document is open, start conversion
                xor     a
                ld      (ImportLine),a
                ld      (ImportLine+1),a

.next_line:
                ; Read next input byte to test for end of file
                call    PrintImportLine
                ld      de,MonitorBuffer

.l1:
                call    fReadByte               ; A = next byte
                jr      z,.finish_read          ; Reached end of file

                ; Check for invalid chars
                cp      $20
                jr      c,.invalid_char
                cp      $80
                jr      c,.valid
.invalid_char:
                ; Check for end of line
                cp      $0d                     ; Reached end of line?
                jr      z,.l1                   ; Ignore $0d, only respond to $0a
                cp      $0a
                jr      z,.finish_read          ; Reached end of line!
                ld      a,' '

.valid:
                ld      (de),a                  ; Write character to buffer
                inc     e
                jr      nz,.l1                  ; Keep going until whole line is read

                ; Run out of bytes in the buffer (line length >= 255), keep overwriting last char until EOL
                dec     e                       ; Go back to the end of the line
                jr      .l1                     ; BTW the last char is lost to null-terminator of buffer any way

.finish_read:
                or      a
                jr      nz,.eol_char            ; enforce tokenising line even when empty
                cp      e
                jr      z,.done                 ; no more input in the file and in buffer -> done
.eol_char:

                ; tokenise the input buffer
                xor     a
                ld      (de),a                  ; add null-terminator to input buffer
                ld      e,a                     ; DE = MonitorBuffer
                push    hl
                ld      hl,de                   ; HL = MonitorBuffer -> tokenise into same buffer
                call    tokeniseLine

                ; insert the line into document
                ld      e,0                     ; DE = MonitorBuffer
                ld      hl,(MainDoc.Length)
                dec     hl
                dec     hl                      ; HL = pointer at last line of document
                call    docInsertLine
                pop     hl                      ; file buffer ptr
                jr      nc,.next_line

                ; insert line or docNew failed -> out of memory / file is too large
.oom
                call    fClose
                call    errorPrint
                dz      "OUT OF MEMORY"
                ret

.done:
                call    fClose
                ld      a,kInkSuccess
                call    colouredPrint
                dz      C_CLEARLINE,"Text file loaded",C_ENTER
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdTextSave
;; Write out the source document as a text file
;;
;; Args:
;;      <filename>
;;

ImportLine      dw      0
PrintImportLine:
                push    af,bc,de,hl
                call    pageVideo
                call    print
                dz      "Line "
                ld      e,0
                ld      hl,(ImportLine)
                inc     hl
                ld      (ImportLine),hl
                dec     hl
                call    unpackD24
                call    insertOff
                call    printBCD5
                call    insertRestore
                call    print
                dz      C_ENTER,C_UP
                pop     hl,de,bc,af
                ret

cmdTextSave:
                call    checkForDoc
                ret     c

                call    cmdExpectFileName
                ret     c
                call    fOpenSave
                ret     c

                ld      hl,kDocStart
                xor     a
                ld      (ImportLine),a
                ld      (ImportLine+1),a
.next_line:
                ; HL = Source folder
                call    PrintImportLine

                ; Check to see if we've reached the end of the source
                call    pageDoc
                ld      a,(hl)
                inc     a
                jr      z,.end

                ; Detokenise current line into MonitorBuffer
                ld      de,MonitorBuffer
                call    detokenise
                ld      bc,MonitorBuffer
                inc     hl                      ; HL = start of next line
                ld      a,$0a
                ld      (de),a                  ; Insert newline at end of detokenised buffer
                inc     de
                ex      de,hl
                and     a
                sbc     hl,bc                   ; HL = length of detokenised string

                ; Write current line in MonitorBuffer to file
                ; DE = input
                ; HL = Length of detokenised buffer
                ;
                ld      bc,hl                   ; BC = number of bytes
                ld      hl,MonitorBuffer
                call    fWrite
                jr      c,.error
                ex      de,hl

                jr      .next_line

.end:
                call    pageVideo
                ld      a,kInkSuccess
                call    colouredPrint
                dz      C_CLEARLINE,"Document exported",C_ENTER
                call    fClose
                jr      c,.error
                ret

.error:
                call    errorPrint
                dz      "UNABLE TO EXPORT FILE"
                ret

.line           dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; cmdMemInfo
;; Dump out information about memory use
;;
;; Args:
;;      None
;;

cmdMemInfo:
                call    cmdExpectEnd
                ret     c
                
                ld      a,kInkResponse
                call    setColour
                call    cursorHide

                ;;
                ;; List out the document pages
                ;;

                call    print
                dz      "Document:", C_ENTER,"  Pages: "
                ld      hl,MainDoc.Pages
                ld      b,6
                call    listPages
                call    print
                dz      "   Size: "
                ld      hl,(MainDoc.Length)
                ld      e,0
                call    unpackD24
                call    printBCD5
                call    print
                dz      C_ENTER

                ;;
                ;; List assembly pages
                ;;

                call    print
                dz      "Code:",C_ENTER,"  Pages: "
                ld      hl,CodePages
                ld      b,5
                call    listPages

                ;;
                ;; List symbol pages
                ;;

                call    print
                dz      "Symbols:",C_ENTER,"  Pages: "
                ld      hl,SymPages
                ld      a,(NumSymPages)
                ld      b,a
                call    listPages
                call    print
                dz      "    Num: "
                ld      hl,(NumSyms)
                ld      e,0
                call    unpackD24
                call    printBCD4
                call    print
                dz      "/2048",C_ENTER

                call    cursorShow
                ld      a,kInkNormal
                call    setColour
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; listPages
;; Print out a list of bytes used for displaying page numbers
;;
;; Input:
;;      HL = array of pages
;;      B = # of pages
;;

listPages:
                ld      a,b
                and     a
                jr      z,.no_pages

                ldi     a,(hl)
                call    printHexByte
                call    printSpace
                djnz    listPages
                ld      a,C_ENTER
                jp      printChar

.no_pages:
                call    print
                dz      "No pages allocated",C_ENTER
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdLS
;; List all the files in a given directory (default is current directory)
;;
;; Args:
;;      <directory> (optional)
;;

cmdLS:
                call    pageVideo
                call    resetLineCount
                ld      a,(de)
                and     a
                jr      z,.current_folder

                ; Put parameter in FileName buffer
                call    cmdExpectString
                ret     c
                call    cmdExpectEnd
                ret     c
                ld      de,FileName
                call    strSliceCopy
                jr      .ls

.current_folder:
                ; Get current directory
                call    fGetCwd
.ls:
                ei                      ; We will need keyboard
                ld      a,'*'
                ld      b,$10           ; Long filenames returned
                ld      hl,FileName
                FILE    F_OPENDIR
                jp      c,.error
                ld      (.handle),a

.l0:
                ld      a,(.handle)
                ld      hl,FileName
                FILE    F_READDIR
                jr      c,.error_close
                and     a               ; ZF = 1 if no more files
                jr      z,.end

                ; In buffer at HL, we have:
                ;
                ; Byte 0 = attributes:
                ;               Read-only:      $01
                ;               Hidden:         $02
                ;               System:         $04
                ;               Volume ID:      $08
                ;               Directory:      $10
                ;               Archive:        $20
                ; Byte 1 = Filename (null terminated)
                ; Next 2 bytes: time stamp
                ; Next 2 bytes: date stamp
                ; Next 4 bytes: file size
                ;
                ; Date format:
                ;       Bits 0-4:       day of month
                ;       Bits 5-8:       month of year
                ;       Bits 9-15:      Years since 1980 (latest year is 2107)
                ;
                ; Time format:
                ;       Bits 0-4:       2 second count (range from 0-29, which is 0-58 secs)
                ;       Bits 5-10:      Minutes (0-59)
                ;       Bits 11-15:     Hours (0-23)
                ;


                ; We should only list files that are not hidden, system or volume ID entries

                ld      hl,FileName
                ld      a,(hl)
                and     %00'001110      ; ZF = 1 means that this file is OK
                jr      nz,.l0          ; But this one isn't so go to next one!

                ld      a,(hl)
                inc     hl              ; Point to filename
                push    hl              ; Store filename position
                and     $10             ; ZF = 1 if normal file
                jr      z,.file

                ; Directory entry
                ld      a,kInkDirectory
                jr      .show_file
.file:
                ; Find the next '.' or null terminator
.l1:
                ld      a,(hl)
                cp      '.'
                jr      z,.ext_found
                and     a
                jr      z,.norm_file
                inc     hl
                jr      .l1
.ext_found:
                ; We know we have at least one dot in our filename
                call    strEnd          ; Jump to end of filename
.l2:
                dec     hl
                ld      a,(hl)
                cp      '.'
                jr      nz,.l2
                
                ; HL points to extension (is it .odn?)
                inc     hl

                ; Test first character of extension
                ldi     a,(hl)
                res     5,a             ; Convert to upper case
                cp      'O'
                jr      nz,.norm_file

                ; Test first character of extension
                ldi     a,(hl)
                res     5,a             ; Convert to upper case
                cp      'D'
                jr      nz,.norm_file

                ; Test first character of extension
                ldi     a,(hl)
                res     5,a             ; Convert to upper case
                cp      'N'
                jr      nz,.norm_file
.odn_file:
                ld      a,kInkOdinFile
                jr      .show_file
.norm_file:
                ld      a,kInkNormalFile
.show_file:
                pop     hl                      ; Restore filename position

                call    setColour
                call    printHL                 ; Print filename
                ld      a,kInkNormal
                call    setColour
                ld      a,C_CLEARTOEND
                call    printChar               ; clear rest of the line
                ld      a,C_ENTER
                call    printChar
                call    checkLineCount
                jr      nz,.l0

.end:
                ld      a,(.handle)
                FILE    F_CLOSE
                ret

.error_close:
                ld      a,(.handle)
                FILE    F_CLOSE
.error:
                call    errorPrint
                dz      "COULD NOT READ FOLDER"
                ret

.handle         db      0

;;----------------------------------------------------------------------------------------------------------------------
;; cmdCD
;; Change directory.  If no directory is given, it shows the current directory.
;;
;; Args:
;;      <directory> (optional)
;;

cmdCD:
                call    pageVideo
                ld      a,(de)
                and     a
                jr      nz,.change_dir

                ; Just show the current directory
.show_dir:
                ld      a,'*'
                ld      hl,FileName
                FILE    F_GETCWD
                jr      c,.error_pwd

                ld      a,kInkDirectory
                call    setColour
                ld      hl,FileName
                call    printHL
                ld      a,C_ENTER
                call    printChar
                ld      a,kInkNormal
                call    setColour
                ret
.error_pwd:
                call    errorPrint
                dz      "UNABLE TO GET CURRENT DIRECTORY"
                ret

.change_dir:
                call    cmdExpectString
                ret     c
                call    cmdExpectEnd
                ret     c
                ld      de,FileName
                call    strSliceCopy

                ld      hl,FileName

                ; Check to see if we have a drive
                inc     hl
                ld      a,(hl)
                dec     hl
                cp      ':'
                jr      nz,.no_drive

                ; Change drive
                ld      a,(hl)
                res     5,a             ; make it lowercase (assuming it's a letter)
                sub     'A'
                jr      c,.error_bad_drive
                cp      16
                jr      nc,.error_bad_drive
                add     a,a
                add     a,a
                add     a,a
                or      1               ; A = drive code
                FILE    M_GETSETDRV
                jr      nc,.no_drive
.error_bad_drive:
                call    errorPrint
                dz      "BAD DRIVE"
                ret
.no_drive:
                ld      a,'*'
                ld      hl,FileName
                FILE    F_CHDIR
                jr      c,.error_cd

                jp      .show_dir
.error_cd:
                call    errorPrint
                dz      "UNABLE TO CHANGE DIRECTORY"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdMD
;; Create a directory
;;
;; Args:
;;      <directory>
;;

cmdMD:
                call    pageVideo
                call    cmdExpectString
                ret     c
                call    cmdExpectEnd
                ret     c

                ld      de,FileName
                call    strSliceCopy
                ld      a,'*'
                ld      hl,FileName
                FILE    F_MKDIR
                jr      nc,.no_error

                call    errorPrint
                dz      "COULD NOT CREATE DIRECTORY"
                ret
.no_error:
                ld      a,kInkSuccess
                call    setColour
                call    print
                dz      "Created",C_ENTER
                ld      a,kInkNormal
                call    setColour
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdRemount
;; Remount the SD card
;;
;; Args:
;;      None
;;

cmdRemount:
                call    pageDivMMC
                ld      a,1
                call    callP3dos
                dw      IDE_MOUNT
                ret     c
                call    errorPrint
                dz      "ERROR: Could not remount drives"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdDump
;; Memory dump
;;
;; Args:
;;      <Address> (optional)
;;

DumpAddr        dw      $6000

cmdDump:
                call    insertOff
                ld      hl,(DumpAddr)
                ld      a,(de)
                and     a
                jr      z,.no_addr

                call    cmdExpectAddress
                ret     c
.no_addr:
                call    pageVideo
                ld      c,8             ; eight lines
.next_line:
                ; Grab the data from memory
                ; HL = PPPAAAAA AAAAAAAA
                push    hl

                call    fillMonitorBuffer

                ; Print out the data
                ;           1         2         3         4         5         6         7
                ; 0---------0---------0---------0---------0---------0---------0---------0---------
                ; PP:OOOO AAAA  XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX  ................

                ; Print 24-bit address
                ld      a,kInkAddr24
                call    setColour
.page+1:        ld      a,$CC
                call    printHexByte
                ld      a,':'
                call    printChar
                call    printHexWord    ; HL should be offset within page here
                call    printSpace

                ; Print 16-bit address
                ld      a,kInkAddr16
                call    setColour
                pop     hl              ; HL = DumpAddr
                push    hl
                call    printHexWord
                call    dumpLine

                ; Next line
                pop     hl
                add     hl,16
                ld      (DumpAddr),hl

                dec     c
                jp      nz,.next_line
                jp      insertRestore   ; exit DumpAddr

fillMonitorBuffer:
                ; Put 16 bytes from the VM to the monitor buffer
                ld      a,h
                ld      e,a
                and     $1f
                ld      h,a             ; HL = actual address to read (offset within page)
                xor     e
                rlca
                rlca
                rlca                    ; A = slot index
                ld      de,MMUState
                add     de,a
                ldi     a,(de)          ; A = page number in current MMU state
                page    4,a
                ld      (cmdDump.page),a
                ld      a,(de)
                page    5,a

                ld      de,MonitorBuffer
                push    hl,bc
                add     hl,$8000        ; Read from MMU 4
                ld      bc,16
                ldir
                pop     bc,hl
                ret

dumpLine:
                ; HL = address
                call    printSpace
                call    printSpace

                ; Print bytes
                ld      b,16
                ld      hl,MonitorBuffer
.next_byte:
                call    .sel_color
                call    setColour
                ldi     a,(hl)
                call    printHexByte
                call    printSpace
                djnz    .next_byte

                call    printSpace

                ; Print characters
                ld      l,b         ; HL = MonitorBuffer (L == 0)
                ld      b,16
.next_char:
                call    .sel_color
; dump variant "Simon" - change colours of $80..$FF values
;                 bit     7,(hl)
;                 jr      z,.seven_bit_char
;                 xor     %011'1'000
; .seven_bit_char:
                call    setColour
                ldi     a,(hl)
; dump variant "Simon" - make $80..$FF values display as 7b ASCII
;                 and     $7F
                call    makePrintable
                call    printChar
                djnz    .next_char

                ld      a,kInkNormal
                call    setColour

                ld      a,C_ENTER
                jp      printChar

.sel_color:             ; select color based on the L, grouping it by 4 bytes
                ld      a,kInkBytes
                bit     2,l
                ret     nz
                dec     a
                ret


;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
