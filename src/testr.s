;;----------------------------------------------------------------------------------------------------------------------
;; Odin Assembler - automated tests
;;----------------------------------------------------------------------------------------------------------------------

                DEVICE          ZXSPECTRUMNEXT
                CSPECTMAP       "testr.map"

;;----------------------------------------------------------------------------------------------------------------------
;; Memory map
;;
;;      +---------------------------------------+ 0000
;;      | p?  dynamic workspace (symbols, etc)  |
;;      +---------------------------------------+ 2000
;;      |                                       |
;;      +---------------------------------------+ 4000
;;      | p10 tilemode VRAM, mem allocator data |
;;      +---------------------------------------+ 6000
;;      | p11 tilemode font (killing sysvars)   |
;;      +---------------------------------------+ 8000
;;      | p4  Like "user" space in Odin         |
;;      +---------------------------------------+ a000
;;      | p5  Test runner                       |
;;      +---------------------------------------+ c000
;;      | p20 Shared code and stack             |
;;      +---------------------------------------+ e000
;;      | Paged code (other modules)            |
;;      +---------------------------------------+
;;
;; Page usage:
;;  20 = shared code (assembled for $c000)
;;  21 = monitor (assembled for $e000)
;;  22 = editor (assembled for $e000)
;;  23 = assembler (assembled for $e000)
;;  24 = debugger (assembled for $0000) (25 = debugger data)
;;  32..63 = pages used by test-allocator in allocPage implementation stub
;;  64+ = pages used by fake documents for document.s tests
;;

    ; test-runner stubs for some global functions
    define allocPage __testAllocPage : define freePage __testFreePage : define errorPrint __testErrorPrint

;;----------------------------------------------------------------------------------------------------------------------
;; Includes

                include "src/shared/defines.s"
                include "src/shared/consts.s"

                ;;
                ;; Shared code
                ;;

                MMU     6,20,$c000

                include "src/shared/keyboard.s"
    undefine errorPrint
                include "src/shared/console.s"
    define errorPrint __testErrorPrint
                include "src/shared/document.s"
                include "src/shared/filesys.s"
                include "src/shared/maths.s"
                include "src/shared/memory.s"
                include "src/shared/page.s"
                include "src/shared/string.s"
                include "src/shared/tokens.s"
                include "src/shared/utils.s"
                include "src/shared/vars.s"
                include "src/shared/args.s"

                defcallhl
                defcallix

                display "[ SHARED ] Final address (Max = $dcff): ",$," Space: ",$dd00-$," Page: ",/D,$$

                org     $dd00
AsmBuffer
KeyboardBuffer  ds      256

TokenisedBuffer                         ; Buffer used for tokenising line while using the editor
ExprBuffer                              ; Used for calculating expressions during assembly
FileBuffer      ds      256             ; Used for loading and saving

Stack           ds      256
StackTop        ASSERT $e000 == StackTop

                ;;
                ;; Monitor
                ;;

                MMU     7,21,$e000

MonitorBuffer   ds      256             ; must be 256 aligned and not at end of RAM (!$FF00)

                include "src/monitor/commands.s"
                include "src/monitor/monitor.s"

                display "[MONITOR ] Final address (Max = $ffff): ",$," Space: ",$10000-$," Page: ",/D,$$

                ;;
                ;; Editor
                ;;

                MMU     7,22,$e000

EditorBuffer    ds      256             ; must be 256 aligned and not at end of RAM (!$FF00)

                include "src/editor/buffer.s"
                include "src/editor/cmdtable.s"
                include "src/editor/editor.s"
                include "src/editor/switch.s"
                include "src/editor/clipboard.s"

                display "[ EDITOR ] Final address (Max = $ffff): ",$," Space: ",$10000-$," Page: ",/D,$$

                ;;
                ;; Assembler
                ;;

                MMU     7,23,$e000

                include "src/asm/symbols.s"
                include "src/asm/lex.s"
                include "src/asm/expr.s"
                include "src/asm/ld.s"
                include "src/asm/asm.s"
                include "src/asm/emit.s"
                include "src/asm/opt.s"
                include "src/asm/table.s"

                display "[  ASM   ] Final Address (Max = $ffff): ",$," Space: ",$10000-$," Page: ",/D,$$

                ;;
                ;; Debugger
                ;;

                MMU     0,24    ; `org 0` is done in debugger.s

                include "src/debugger/debugger.s"

                display "[DEBUGGER] Final Address (Max = $1fff): ",$," Space: ",$2000-$," Page: ",/D,$$

;;----------------------------------------------------------------------------------------------------------------------
;; Test runner

                ORG     $a000

Start:
                ;;------------------------------------------------------------------------------------------------------
                ;; Set up the test runner
                ;;------------------------------------------------------------------------------------------------------

                nextreg $07,3           ; 28MHz mode

                ; Set the test page #s
                page    6,$$SharedCode
                ld      de,SharedCode
                ld      hl,__TestPages
                ld      bc,NUM_CODE_PAGES
                ldir

                ; switch to shared+monitor memory map
                ld      a,(SharedCode)
                page    6,a
                ld      a,(MonitorCode)
                page    7,a

                ; init gfx mode
                call    modeConsole         ; does also reset cursor to [0,0]
                ld a,6 : call colouredPrint : dz "Odin test runner - running tests... Alloc pg [                                ]"
                                ; this string works also as page allocation tracking directly in the VRAM ^^^ (do NOT move)

        ;----------------------------------------------------------------------------------------------------------------------
        ; run test-runner routines of all [available] modules

        ;----------------------------------------------------------------------------------------------------------------------
        ; suite 1: console.s, symbols.s, string.s, tokens.s, utils.s, document.s (very full memory)
        IFDEF __T_SUITE_1
                call    __test_console_s    ; does include also couple of memory_s tests
                call    __test_string_s
                call    __test_tokens_s
                call    __test_utils_s
                call    __test_document_s
        ENDIF

        ;----------------------------------------------------------------------------------------------------------------------
        ; suite 2: self-test, document.s, symbols.s, buffer.s, asm.s, ld.s, opt.s, expr.s, lex.s
        IFDEF __T_SUITE_2
                call    __test_testr_self
                call    __test_document_s_2
                call    __test_symbols_s
                call    __test_buffer_s
                call    __test_asm_s
                call    __test_expr_s
                call    __test_lex_s
        ENDIF

        ;----------------------------------------------------------------------------------------------------------------------
        ; suite 3:
        IFDEF __T_SUITE_3
                call    __test_expr_s_2
                call    __test_maths_s
        ENDIF

        ;----------------------------------------------------------------------------------------------------------------------
        ; display FINISHED
                ld a,(__t_total_res) : call colouredPrint : dz C_ENTER,"FINISHED"
                jr      $

__T_SETUP       MACRO   letter?
                    ld      a,letter?
                    call    __t_setup_impl
                ENDM

__T_CHECK       MACRO   cond?
                    call    __t_check_cond?
                ENDM

__T_BYTECMP     MACRO   adr?, value?
                    call    __t_cmp_byte
                    dw      adr?
                    db      value?
                ENDM

__T_WORDCMP     MACRO   adr?, value?
                    call    __t_cmp_word
                    dw      adr?
                    dw      value?
                ENDM

__T_MEMCMP      MACRO   adr?, data?
                    call    __t_cmp_mem
                    dw      adr?
                    db      .data_e - .data_s
.data_s             db      data?
.data_e             ASSERT 1 <= (.data_e - .data_s) && (.data_e - .data_s) <= 255
                    ASSERT 1 != (.data_e - .data_s), "use __T_BYTECMP instead"
                    ASSERT 2 != (.data_e - .data_s), "use __T_WORDCMP instead"
                ENDM

__T_EXPECT_ERR  MACRO
                    call    __t_dec_errPrint
                ENDM

__T_STRCPY      MACRO   adr?, string?
                    call    __t_setup_strcpy
                    dw      adr?
                    dz      string?
                ENDM

__T_MEMCPY      MACRO   adr?, data?
                    call    __t_setup_memcpy
                    dw      adr?
                    db      .data_e - .data_s
.data_s             db      data?
.data_e             ASSERT 3 <= (.data_e - .data_s) && (.data_e - .data_s) <= 255
                ENDM

;;----------------------------------------------------------------------------------------------------------------------
; test harness variables, init values

__TestPages:    db      20, 21, 22, 23, 24, 25
__T_FREE_PAGE_FIRST     equ     32                      ; the allocation map is in VRAM (!)
__T_FREE_PAGE_END       equ     __T_FREE_PAGE_FIRST+31
__t_total_res   db      $04
__t_result:     db      $ee
__t_test_name:  db      '~'
__t_errPrintCnt db      123

__t_default_a   equ     $EF
__t_default_bc  equ     $DDCC
__t_default_de  equ     $BBAA
__t_default_hl  equ     $9988

;;----------------------------------------------------------------------------------------------------------------------
;; Test runner utility functions
;; __t_display_result       -> display result (char from `__t_test_name` with `__t_result` color) (checks also "errorPrint" call-count)
;; __t_cmp_set_err          -> set result of current test as fail (and also global result)
;; __t_cmp_**what**         -> compare of values returning back to caller (partial check without display)
;; __t_check_**cond**       -> complete check ending with __t_display_result, parsing arguments (cheks if default values were preserved)
;; __t_check_only_**cond**  -> complete check ending with __t_display_result, parsing arguments (doesn't check default values preservd)

__t_setup_impl:
                ld (__t_test_name),a : exa : ld a,4 : ld (__t_result),a ; green color of result
                ld a,6 : call setColour : exa : call printChar      ; print test name in yellow to signal execution
                xor a : ld (__t_errPrintCnt),a                      ; reset "errorPrint" call-counter
                ld a,__t_default_a : ld bc,__t_default_bc : ld de,__t_default_de : ld hl,__t_default_hl
                cp a : scf          ; ZF=1, SF=0, CF=1
                ret

__t_setup_strcpy:       ; arguments after CALL: DW adress, DZ string
                ex      (sp),hl                 ; preserve HL, HL = arguments for strcpy
                push    af,de
                ldi     de,(hl)                 ; DE = dest.adr, HL = string
                call    strcpy
                pop     de,af
                ex      (sp),hl                 ; restore HL and set return address after arguments
                ret

__t_setup_memcpy:       ; arguments after CALL: DW adress, DB size, DB data
                ex      (sp),hl
                push    af,de,bc
                ldi     de,(hl)                 ; DE = dest.adr, HL = string
                ldi     c,(hl)
                ld      b,0
                call    memcpy
                add     hl,bc
                pop     bc,de,af
                ex      (sp),hl                 ; restore HL and set return address after arguments
                ret

__t_check_none: ; nothing to do, continue with __t_display_result
__t_display_result:                             ; this DOES check if __t_errPrintCnt is zero
                ld      a,(__t_errPrintCnt)
                or      a
                call    nz,__t_cmp_set_err
                ld      a,(__t_result)
                call    setColour
                ld      a,C_BACKSPACE
                call    printChar               ; remove the "execution" char and overwrite it with green name
                ld      a,(__t_test_name)
                jp      printChar

__t_cmp_set_err:                                ; red color of result and total result
                push    af
                ld a,2 : ld (__t_result),a : ld (__t_total_res),a
                pop     af
                ret

__t_dec_errPrint:                               ; decrement print-error counter by one, preserve everything else
                push    af
                ld      a,(__t_errPrintCnt)
                dec     a
                ld      (__t_errPrintCnt),a
                pop     af
                ret

; - check routines for various conditions ------------------------------------------------------------------------------

__t_cmp_cf:
                ret     c
                jr      __t_cmp_set_err

__t_cmp_ncf:
                ret     nc
                jr      __t_cmp_set_err

__t_cmp_zf:
                ret     z
                jr      __t_cmp_set_err

__t_cmp_nzf:
                ret     nz
                jr      __t_cmp_set_err

__t_cmp_sf:
                ret     m
                jr      __t_cmp_set_err

__t_cmp_nsf:
                ret     p
                jr      __t_cmp_set_err

__t_cmp_default_flags:
                call    __t_cmp_nsf             ; default flags from test init are CF=1, ZF=1, SF=0
                call    __t_cmp_zf
                jr      __t_cmp_cf

__t_cmp_default_flags_a:
                call    __t_cmp_default_flags
__t_cmp_default_a:
                cp      __t_default_a
                ret     z
                jr      __t_cmp_set_err

__t_cmp_default_a_de:
                call    __t_cmp_default_a
__t_cmp_default_de:
                add     de,-__t_default_de+257  ; = 257 from default value (d = 1, e = 1)
                dec     e
                jr      nz,__t_cmp_set_err
                dec     d
                jr      nz,__t_cmp_set_err
                ret

__t_cmp_default_a_bc:
                call    __t_cmp_default_a
                jr      __t_cmp_default_bc

__t_cmp_default_a_bc_de:
                call    __t_cmp_default_a_de
__t_cmp_default_bc:
                add     bc,-__t_default_bc+257  ; = 257 from default value (b = 1, c = 1)
                djnz    __t_cmp_set_err         ; if b != 1
                dec     c
                jr      nz,__t_cmp_set_err      ; if c != 1
                ret

__t_cmp_default_bc_de_hl:
                call    __t_cmp_default_hl
__t_cmp_default_bc_de:
                call    __t_cmp_default_de
                jr      __t_cmp_default_bc

__t_cmp_default_a_bc_hl:
                call    __t_cmp_default_a
__t_cmp_default_bc_hl:
                call    __t_cmp_default_bc
__t_cmp_default_hl:
                add     hl,-__t_default_hl+257  ; = 257 from default value (d = 1, e = 1)
                dec     l
                jr      nz,__t_cmp_set_err
                dec     h
                jr      nz,__t_cmp_set_err
                ret

__t_cmp_a_equal_argHL:                          ; checks A vs [HL] and HL += 1
                cp (hl) : inc hl
                jr      nz,__t_cmp_set_err
                ret

__t_cmp_bc_equal_argHL:                         ; checks BC vs [HL] and HL += 2
                ld a,c : cp (hl) : inc hl
                call    nz,__t_cmp_set_err
                ld a,b : cp (hl) : inc hl
                jp      nz,__t_cmp_set_err
                ret

__t_cmp_de_equal_argHL:                         ; checks DE vs [HL] and HL += 2
                ld a,e : cp (hl) : inc hl
                call    nz,__t_cmp_set_err
                ld a,d : cp (hl) : inc hl
                jp      nz,__t_cmp_set_err
                ret

__t_cmp_mem:    ; arguments after CALL: word adress, byte length, length * byte = data
                ex      (sp),hl                 ; preserve HL, HL = arguments for cmp_mem
                push    af,bc,de
                ldi     de,(hl)
                ldi     c,(hl)
                ld      b,0
                call    memcmp
                call    nz,__t_cmp_set_err
                add     hl,bc
                pop     de,bc,af
                ex      (sp),hl                 ; restore HL and set return address after arguments
                ret

__t_cmp_byte:
                ex      (sp),hl
                push    af,de
                ldi     de,(hl)
                ld      a,(de)
                call    __t_cmp_a_equal_argHL
                pop     de,af
                ex      (sp),hl                 ; restore HL and set return address after arguments
                ret

__t_cmp_word:
                ex      (sp),hl
                push    af,de
                ldi     de,(hl)
                ldi     a,(de)
                call    __t_cmp_a_equal_argHL
                ld      a,(de)
                call    __t_cmp_a_equal_argHL
                pop     de,af
                ex      (sp),hl                 ; restore HL and set return address after arguments
                ret

; - complete checks after test, calling partial checks and displaying result -------------------------------------------

; TODO re-organize... (to shorten the code and normalize the check combinations a bit, although there will be still lot of them)
; start with "only_a_bc_de_hl" (or whichever order of r16s is best)
;  -> add "fromHL" extra entry points
;  -> add wrappers for "only_bc_de_hl", "only_de_hl", "only_hl"
;  -> add default_reg + a_bc_de_hl tailing into only_... for rest of checks
;  -> add flags*_* ... variants then

__t_check_errorPrintCnt1:
                ld      hl,__t_errPrintCnt
                dec     (hl)
                jp      __t_check_none

__t_check_all:
                call    __t_cmp_default_flags
__t_check_regs:
                call    __t_cmp_default_a
                call    __t_cmp_default_bc_de_hl
                jp      __t_display_result

__t_check_ncf:
                ccf
__t_check_cf:
                call    __t_cmp_cf              ; check CF
                jp      __t_display_result

__t_check_ncf_hl:
                ccf
                jr      __t_check_cf_hl
__t_check_flags_hl:
                call    __t_cmp_nsf             ; default flags from test init are CF=1, ZF=1, SF=0
                call    __t_cmp_zf
__t_check_cf_hl:
                call    __t_cmp_cf              ; check CF
__t_check_hl:
                call    __t_cmp_default_a_bc_de ; check A, BC, DE were preserved
                ex      de,hl                   ; check HL against arg1
                pop     hl
                call    __t_cmp_de_equal_argHL
                call    __t_display_result
                jp      hl                      ; return back to code

__t_check_flags_hl_bc:
                call    __t_cmp_nsf             ; default flags from test init are CF=1, ZF=1, SF=0
                call    __t_cmp_zf
__t_check_cf_hl_bc:
                call    __t_cmp_cf              ; check CF
                call    __t_cmp_default_a_de    ; check A, DE were preserved
                ex      de,hl                   ; check HL against arg1
                pop     hl
.only_de_bc_fromHL:     ; internal entry point: __t_check_cf_hl_bc.only_de_bc_fromHL
                call    __t_cmp_de_equal_argHL
                call    __t_cmp_bc_equal_argHL  ; check BC against arg2
                call    __t_display_result
                jp      hl                      ; return back to code

__t_check_only_ncf_hl_bc:
                call    __t_cmp_ncf             ; check NCF
                jr      __t_check_only_hl_bc

__t_check_ncf_hl_bc:
                call    __t_cmp_ncf             ; check NCF
                call    __t_cmp_default_a_de    ; check A, DE were preserved
__t_check_only_hl_bc:
                ex      de,hl                   ; check HL against arg1
                pop     hl
                jr      __t_check_cf_hl_bc.only_de_bc_fromHL

__t_check_ncf_zf_a_de_bc:
                ccf
__t_check_cf_zf_a_de_bc:
                call    __t_cmp_cf
                call    __t_cmp_zf
                jr      __t_check_a_de_bc

__t_check_flags_a_de_bc:
                call    __t_cmp_default_flags
__t_check_a_de_bc:
                call    __t_cmp_default_hl
.only_a_de_bc:
                pop     hl
                call    __t_cmp_a_equal_argHL   ; check A against arg1
                jr      __t_check_cf_hl_bc.only_de_bc_fromHL

__t_check_ncf_a_hl_bc:
                ccf
                jr      __t_check_cf_a_hl_bc
__t_check_flags_a_hl_bc:
                call    __t_cmp_default_flags
__t_check_cf_a_hl_bc:
                call    __t_cmp_cf              ; check CF
__t_check_a_hl_bc:
                call    __t_cmp_default_de
                ex      de,hl
                jr      __t_check_a_de_bc.only_a_de_bc

__t_check_cf_de:
                call    __t_cmp_cf              ; check CF
                call    __t_cmp_default_a_bc_hl
                jr      __t_check_only_de

__t_check_only_ncf_nzf_a_de:
                ccf
__t_check_only_cf_nzf_a_de:
                call    __t_cmp_cf              ; check CF
                call    __t_cmp_nzf             ; check NZF
                jr      __t_check_only_a_de

__t_check_ncf_a_de:
                ccf
__t_check_cf_a_de:
                call    __t_cmp_cf              ; check CF
__t_check_a_de:
                call    __t_cmp_default_bc_hl   ; check BC, HL were preserved
__t_check_only_a_de:
                pop     hl                      ; check A against arg1
                call    __t_cmp_a_equal_argHL
                call    __t_cmp_de_equal_argHL  ; check DE against arg2
                call    __t_display_result
                jp      hl                      ; return back to code

__t_check_nzf_a:
                call    __t_cmp_nzf
                jr      __t_check_a
__t_check_zf_a:
                call    __t_cmp_zf
                jr      __t_check_a
__t_check_ncf_a:
                ccf
__t_check_cf_a:
                call    __t_cmp_cf              ; check CF
__t_check_a:
                call    __t_cmp_default_bc_de_hl
__t_check_only_a:
                pop     hl                      ; check A against arg1
                call    __t_cmp_a_equal_argHL
                call    __t_display_result
                jp      hl                      ; return back to code

__t_check_cf_a_hl_de:
                ccf
__t_check_ncf_a_hl_de:
                call    __t_cmp_ncf
                jr      __t_check_a_hl_de

__t_check_zf_a_hl_de:
                call    __t_cmp_zf
__t_check_a_hl_de:
                call    __t_cmp_default_bc      ; check BC was preserved
                ex      (sp),hl                 ; HL to stack, get address of arguments
                call    __t_cmp_a_equal_argHL   ; check A against arg1
                pop     bc
                call    __t_cmp_bc_equal_argHL  ; check HL (in BC) against arg2
                call    __t_cmp_de_equal_argHL  ; check DE against arg3
                call    __t_display_result
                jp      hl

__t_check_ncf_nzf_a_hl_de:
                ccf
__t_check_cf_nzf_a_hl_de:
                call    __t_cmp_cf
__t_check_nzf_a_hl_de:
                call    __t_cmp_nzf
                jr      __t_check_a_hl_de

__t_check_zf_ncf_de_hl:
                call    __t_cmp_zf
__t_check_ncf_de_hl:
                call    __t_cmp_ncf
__t_check_de_hl:
                call    __t_cmp_default_a_bc
                jr      __t_check_only_de_hl

__t_check_nzf_ncf_de_hl:
                call    __t_cmp_nzf
                jr      __t_check_ncf_de_hl

__t_check_nzf_cf_de_hl:
                call    __t_cmp_nzf
__t_check_cf_de_hl:
                ccf
                jr      __t_check_ncf_de_hl

__t_check_only_zf:
                call    __t_cmp_zf
                jp      __t_display_result

__t_check_only_ncf_de:
                ccf
__t_check_only_cf_de:
                call    __t_cmp_cf
__t_check_only_de:
                pop     hl
.fromHl:
                call    __t_cmp_de_equal_argHL
                call    __t_display_result
                jp      hl

__t_check_only_cf:
                call    __t_cmp_cf
                jp      __t_display_result

__t_check_only_ncf:
                call    __t_cmp_ncf
                jp      __t_display_result

__t_check_ncf_a_hl:
                ccf
__t_check_cf_a_hl:
                call    __t_cmp_cf              ; check CFF
                jr      __t_check_a_hl
__t_check_nzf_a_hl:
                call    __t_cmp_nzf             ; check NZF
                jr      __t_check_a_hl
__t_check_zf_a_hl:
                call    __t_cmp_zf              ; check ZF
__t_check_a_hl:
                call    __t_cmp_default_bc_de
__t_check_only_a_hl:
                ex      de,hl
                jp      __t_check_only_a_de

__t_check_only_ncf_de_hl:
                ccf
__t_check_only_cf_de_hl:
                call    __t_cmp_cf              ; check CF
__t_check_only_de_hl:
                ex      (sp),hl                 ; HL to stack, get address of arguments
                call    __t_cmp_de_equal_argHL  ; check DE against arg1
                pop     de
                call    __t_cmp_de_equal_argHL  ; check HL (in DE) against arg2
                call    __t_display_result
                jp      hl                      ; return back to code

__t_check_ncf_de_hl_bc:
                ccf
__t_check_cf_de_hl_bc:
                call    __t_cmp_cf
                cp a : scf                      ; reset flags to default to pass test
__t_check_flags_de_hl_bc:
                call    __t_cmp_default_flags_a
                jr      __t_check_only_de_hl_bc

__t_check_only_cf_de_hl_bc:
                call    __t_cmp_cf              ; check CF
__t_check_only_de_hl_bc:
                ex      (sp),hl                 ; HL to stack, get address of arguments
                call    __t_cmp_de_equal_argHL  ; check DE against arg1
                pop     de
                jp      __t_check_cf_hl_bc.only_de_bc_fromHL

__t_check_only_ncf_de_hl_bc:
                call    __t_cmp_ncf             ; check NCF
                ex      (sp),hl                 ; HL to stack, get address of arguments
                call    __t_cmp_de_equal_argHL  ; check DE against arg1
                pop     de
                jp      __t_check_cf_hl_bc.only_de_bc_fromHL

__t_check_ncf_a_de_hl_bc:
                ccf
__t_check_cf_a_de_hl_bc:
                call    __t_cmp_cf
__t_check_only_a_de_hl_bc:
                ex      (sp),hl                 ; HL to stack, get address of arguments
                call    __t_cmp_a_equal_argHL   ; check A against arg1
                call    __t_cmp_de_equal_argHL  ; check DE against arg2
                pop     de                      ; HL as DE
                jp      __t_check_cf_hl_bc.only_de_bc_fromHL

;;----------------------------------------------------------------------------------------------------------------------
;; Test variants of global utility functions: memory.s, console.s

__testErrorPrint:
                ex      (sp),hl                 ; HL = ptr to print data
                push    hl                      ; increment errorPrint call-counter for test harness
                ld      hl,__t_errPrintCnt
                inc     (hl)
                pop     hl                      ; skip HL to point beyond data (ret address)
.skipPrintData:
                ldi     a,(hl)
                or      a
                jr      nz,.skipPrintData
                ex      (sp),hl                 ; restore original HL + modify return adr
                scf
                ret

__testAllocPage:
                ; find free page (in VRAM)
                push    de,hl
                ld      hl,$4000+46*2
                ld      e,__T_FREE_PAGE_FIRST
.searchFree:
                ld      a,' '
                cp      (hl)
                jr      z,.foundFree
                inc     hl
                inc     hl
                inc     e
                ld      a,e
                cp      __T_FREE_PAGE_END
                jr      c,.searchFree
                ; out of free memory, report OOM
                ld      (hl),'!'
                inc     hl
                ld      (hl),$14
                ld      e,0         ; return zero page in case of OOM
                scf
                jr      .exit
.foundFree:
                ld      a,e
                and     7
                or      '0'         ; CF=0 to report success
                ld      (hl),a
                inc     hl
                ld      (hl),$18    ; mark slot as used by using '0'..'7' char
.exit:
                ld      a,e
                pop     hl,de
                ret

__testFreePage:
                push    af,hl
                ; check if page number is valid test-runner page
                cp      __T_FREE_PAGE_FIRST
                jr      c,.fatal
                cp      __T_FREE_PAGE_END
                jr      nc,.fatal
                ld      hl,$4000+46*2-__T_FREE_PAGE_FIRST*2
                add     hl,a
                add     hl,a
                ld      a,' '
                cp      (hl)
                jr      z,.wasFreeAlready       ; fatal bug, freezing tests
                ld      (hl),a      ; release the page in the VRAM
                pop     hl,af
                ret
.wasFreeAlready:
                ld      (hl),'!'
                inc     hl
                ld      (hl),$14
.fatal:
                break : nop : nop
                jr $

;;----------------------------------------------------------------------------------------------------------------------
;; Include test-runner routines of all [available] modules

        IFDEF __T_SUITE_1
                include "src/tests/t_console.s" ; does include also couple of memory.s tests
                include "src/tests/t_string.s"
                include "src/tests/t_tokens.s"
                include "src/tests/t_utils.s"
                include "src/tests/t_document.s"
        ENDIF

        IFDEF __T_SUITE_2

            ; few self-tests of the runner macros
__test_testr_self:
                ld a,7 : call colouredPrint : dz C_ENTER,"Test runner: "
                ; TEST __T_CHECK
                __T_SETUP 'n'   ; test for "none", ignoring all register/flag changes
                xor     a
                ld      h,a
                ld      l,a
                ld      d,a
                ld      e,a
                ld      b,a
                ld      c,a
                __T_CHECK none
                __T_SETUP 'a'   ; test for "all" right after __T_SETUP, should never fail
                __T_CHECK all

                ;TEST __T_STRCPY + __T_MEMCMP
                __T_SETUP 's'
                __T_STRCPY FileBuffer, "tr"
                __T_MEMCMP FileBuffer, "tr\0"
                __T_CHECK all

                ;TEST __T_MEMCPY + __T_MEMCMP
                __T_SETUP 'm'
                __T_MEMCPY FileBuffer, "\0tr"
                __T_MEMCMP FileBuffer, "\0tr"
                __T_CHECK all

                ret

            ; include other tests belonging to second suite
                include "src/tests/t_document_2.s"
                include "src/tests/t_symbols.s"
                include "src/tests/t_buffer.s"
                include "src/tests/t_asm.s"
                include "src/tests/t_expr.s"
                include "src/tests/t_lex.s"
        ENDIF

        IFDEF __T_SUITE_3
                include "src/tests/t_expr_2.s"
                include "src/tests/t_maths.s"
        ENDIF

;;----------------------------------------------------------------------------------------------------------------------
;; End of code

                display "[TEST RUN] Final address (Max = $bdff): ",$," Space: ",$be00-$," Page: ",/D,$$
                ASSERT $ < $C000            ; the display above is rather "warning"-like, hard limit is $C000

;;----------------------------------------------------------------------------------------------------------------------
;; Data
                ORG     $6000
                incbin  "data/topan.fnt"
                incbin  "data/logo.fnt"

;;----------------------------------------------------------------------------------------------------------------------
;; NEX file generation

        IFDEF __T_SUITE_1
                SAVENEX OPEN "testr1.nex", Start, StackTop : SAVENEX AUTO : SAVENEX CLOSE
        ENDIF
        IFDEF __T_SUITE_2
                SAVENEX OPEN "testr2.nex", Start, StackTop : SAVENEX AUTO : SAVENEX CLOSE
        ENDIF
        IFDEF __T_SUITE_3
                SAVENEX OPEN "testr3.nex", Start, StackTop : SAVENEX AUTO : SAVENEX CLOSE
        ENDIF

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
