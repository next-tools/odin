;;----------------------------------------------------------------------------------------------------------------------
;; Paging system
;;----------------------------------------------------------------------------------------------------------------------

SharedCode:     db      0               ; Page for shared code at $c000-$dfff
MonitorCode:    db      0               ; Page for monitor code at $e000-$ffff
EditorCode:     db      0               ; Page for editor code at $e000-$ffff
AsmCode:        db      0               ; Page for assembler code at $e000-$ffff
DbgCode:        db      0               ; Page for debugger code at $0000-$1fff
Data:           db      0               ; Page for debugger's data at $2000-$3fff

NUM_CODE_PAGES  equ     $-SharedCode    ; Number of entries above

DivMMCPage:     db      0
PageState       ds      32              ; One bit per page assembled too

NexMode         db      0               ; 1 if OPT NEX has been run at least once
TopPage         db      $df             ; Index of highest page in system - TODO: calculate this on set up

PageBuffer      ds      8,0

; Most of the page swaps are in slot 4, so this has a special entry point to save bytes
readPage4:
                ld      a,4
readPage:
                add     a,REG_MMU0
                push    bc
                ld      bc,IO_REG_SELECT
                out     (c),a
                inc     b
                in      a,(c)
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDoc
;; Page the document in  MMU0-5

pageDoc:
                push    af,hl
                xor     a
                out     ($e3),a
                ld      hl,MainDoc.Pages
.l1:
                ld      a,REG_MMU0 - low MainDoc.Pages
                add     a,l                                 ; A = REG_MMU0 .. REG_MMU5 based on HL address
                ld      (.page),a
                cp      REG_MMU5
                ldi     a,(hl)
.page+2         page    0,a
                jr      nz,.l1
                pop     hl,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDivMMC
;; Page in the DivMMC

pageDivMMC:
                push    af
                ld      a,(DivMMCPage)
                out     ($e3),a
                page    0,$ff
                page    1,$ff
                page    2,10
                page    3,11
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageOutDivMMC
;; Ensure that DivMMC RAM and esxDOS RAM is paged out
;;

pageOutDivMMC:
                push    af
                xor     a
                out     ($e3),a
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageVideo
;; Make sure the tilemap is available

pageVideo:
                page    2,10
                page    3,11
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDebugger
;; Page in the debugger at MMU0
;;

pageDebugger:
                push    af
                call    pageOutDivMMC
                ld      a,(DbgCode)
                page    0,a
                ld      a,(Data)
                page    1,a
                ld      (DbgData),a
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; farCall
;; Call a function in a different page.
;;
;; NOTE: the shared module MUST be within MMU 6 when calling this.  It relies on the `JP (HL)` instruction compiled
;; there via the `callhl` macro.
;;
;; Input:
;;      A = page
;;      HL = address ($e000-$ffff)
;;
;; Output:
;;      A = page
;;
;; Affected:
;;      All registers
;;

farCall:
                push    de
                ld      e,a
                rpage   7
                ld      d,a
                ld      (.pages),de     ; Store previous page and current page (D = old, E = new)

                ; Page in and call routine
                ld      a,e
                page    7,a
                pop     de              ; Restore DE
                callhl

                ; Return to caller
                push    de
                ld      de,(.pages)
                ld      a,d
                page    7,a
                ld      a,e
                pop     de
                ret

.pages          dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; asmAlloc
;; Allocate a page during assembly.
;;
;; Output:
;;      A = Page #
;;      CF = 1 if out of memory
;;

asmAlloc:
                jp      allocPage

;;----------------------------------------------------------------------------------------------------------------------
;; asmFree
;; Free a page previously allocated.  Uses OS if NexMode is 0.  Otherwise it's a no-op (deallocations are done at
;; the beginning of an assembly with pageReset).
;;
;; Input:
;;      A = Page #
;;
;; Affects:
;;      AF, AF'
;;

asmFree:
                jp      freePage

;;----------------------------------------------------------------------------------------------------------------------
;; calcHiPage
;; Calculate the highest page # on this machine.
;; NOTE: Requires the presence of NextZXOS and sysvars being paged in
;;

calcHiPage:
                ld      a,($5b69)
                add     a,a
                inc     a
                ld      (TopPage),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pagesStore
;; Copy data in the document into a series of newly allocated pages.  This is required to be in slot 6 as all the other
;; slots are used to do the copying (document in 0..5, pages to copy to at 7).
;;
;; INPUT:
;;      DE = offset into document
;;      HL = number of bytes to copy
;;
;; OUTPUT:
;;      Document will be paged in if there are bytes to copy.
;;      PageBuffer[0..7] = page indexes
;;      CF = 1 if out of memory by HL>48K or not enough pages to allocate.
;;

pagesStore:
                ; 0 bytes?
                ld      a,h
                or      l
                ret     z

                ; Make sure length <= 48K
                ld      a,h
                cp      $c0
                jr      c,.ok       ; Length < $c000, good!
                scf
                ret     nz          ; Length >= $c100, bad!
                ld      a,l
                and     a
                scf
                ret     nz          ; Length != $c000, bad!
.ok:
                ; Clear the page buffer
                push    bc,hl
                ld      hl,PageBuffer
                ld      bc,8
                call    memclear
                pop     hl

                ; Length is good so let's try to allocate the pages.
                ; First we need to calculate how many pages we require.
                call    calcNumPages    ; B = number of pages
                push    hl
                ld      hl,PageBuffer
                call    saveSlots
                call    allocPages
                call    restoreSlots
                pop     hl
                ret     c               ; We failed to allocate the required pages

                ; Now we need to copy the data into the pages
                ld      a,7
                call    readPage
                exa                     ; A' is the page to recover on exit
                push    ix              ; Store IX
                ld      ix,PageBuffer
                push    hl

.next_page:
                ; HL = number of bytes to process
                ; DE = index into document
                ; B = number of pages

                ld      a,(ix)          ; Get next page
                inc     ix
                page    7,a             ; Page in the clipboard page

                ld      bc,$2000        ; Number of bytes to copy
                ld      a,h
                and     $e0             ; ZF = 1 if length < page size ($0000-$1fff)
                jr      nz,.copy        ; Size >= $2000, so only copy $2000 bytes
                ld      bc,hl           ; We have a partial copy (HL < $2000)
.copy:
                sbc     hl,bc           ; HL = bytes left to do afterwards

                ; Copy the page
                push    hl
                ex      de,hl           ; HL = source in document, DE = number of bytes left
                ld      de,addrMMU7     ; Copy to slot 7
                call    memcpy
                pop     de              ; Restore length in DE
                add     hl,bc           ; Move offset in document
                ex      de,hl

                ld      a,h
                or      l
                jr      nz,.next_page   ; Repeat if we have more work to do

                exa
                page    7,a             ; Restore page in slot 7
.done:
                pop     hl,ix,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pagesRestore
;; Restore pages stored in PageBuffer to an address.  This is required to be in slot 6 as all the other slots are used 
;; to do the copying (document in 0..5, pages to copy to at 7).
;;
;; INPUT:
;;      DE = address to write to
;;      HL = number of bytes
;;

pagesRestore:
                ; Store slot 7 in A'
                ld      a,7
                call    readPage
                exa

                push    bc,de,hl,ix
                ld      ix,PageBuffer

.next_page:
                ld      a,(ix+0)
                inc     ix
                page    7,a

                ld      bc,$2000
                ld      a,h
                and     $e0
                jr      nz,.copy
                ld      bc,hl
.copy:
                ; BC = number of bytes to copy
                ; DE = address to write to
                push    hl
                ld      hl,addrMMU7
                call    memcpy
                pop     hl
                sbc     hl,bc
                ld      a,h
                or      l
                jr      nz,.next_page

                exa
                page    7,a         ; Restore slot 7
                pop     ix,hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; calcNumPages
;; Calculates the number of pages required to hold a number of bytes.
;;
;; INPUT:
;;      HL = number of bytes
;;
;; OUTPUT:
;;      B = number of pages
;;

calcNumPages:
                push    hl
                call    alignUp8K
                ld      a,h
                pop     hl
                and     $e0
                swap
                rrca
                ld      b,a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
