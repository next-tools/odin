;;----------------------------------------------------------------------------------------------------------------------
;; Lexical analysis and tokens
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; tokenise
;; Tokenise the next token in the input according to the given table
;;
;; Input:
;;      DE = input
;;      IX = end-of-token check routine (one of the two, they have own token table pointers):
;;          1) tokenise_is_ins - checks if space or null follows token (consumes space) (has instructions tokens)
;;          2) tokenise_is_op  - if token ends with id-char, input can't follow with id-char (has operand tokens)
;;
;; Output:
;;      CF = 1 if not found (HL,BC modified, DE = input)
;;      CF = 0 if found
;;      HL = Input buffer
;;      DE = New position within input buffer after token
;;      BC = index:sub-index of token
;;
;; Affects:
;;      AF
;;

tokenise:
                ld      hl,(ix-2)       ; load correct token table (the check routine has it)
                jr      .entry
.next_word:
                pop     de
                ret     c               ; Input character < token char?  We didn't find it
.next:
                inc     hl
                bit     7,(hl)
                jr      z,.next
.entry:
                ldi     a,(hl)
                ld      b,a             ; B = current token index
                add     a,1             ; test on end-of-table OP_UNKNOWN
                ret     c               ; return with CF = 1 when end of token table is reached
                ldi     c,(hl)          ; C = current token sub-index

                ld      a,(de)          ; Get first character
                call    upperCase
                cp      (hl)            ; Compare with first character in table
                ret     c               ; First input character < table entry?  We didn't find it
                jr      nz,.next        ; First input character > table entry?  Advance to next token

                ; Input and token have same first character, check rest of it
                push    de              ; Store start of input buffer
.l1:
                inc     de              ; On to next input character
                inc     hl              ; Next character in token
                ld      a,(de)
                call    upperCase
                cp      (hl)
                jr      z,.l1           ; They're still matching - keep going

                or      a               ; clear CF to ensure the next token will be tried (2nd+ letter may be not sorted)
                bit     7,(hl)
                jr      z,.next_word    ; Token char mismatch, try next token
                dec     hl              ; make HL point to last token char again (for "nxt2" and for check routines)

                ; Token and Input did match fully - do the end-of-token check (based on which table is used)
                ; instruction-token: following input must be space or null, space is consumed
                ; operand-token: if last token char is id-char, id-char can't follow
                callix                  ; if CF = 0 is returned, the end condition failed, try next token
                jr      nc,.next_word   ; needs CF = 0 to try next token ("B" -> "BC" -> ...)

                pop     hl              ; HL = input buffer
                or      a               ; CF = 0 if found
                ret

;; check if following input char after token keyword is space, null or semicolon
;; the space is consumed if not followed by null, semicolon or second space (becomes implicit space)
;; returns CF = 1 when success (instruction-token does match)
                dw      InsTokenTable   ; address of my token table (at -2 address of routine)
tokenise_is_ins:
                ; A = following input character (at DE)
                cp      ' '             ; if not space, then whole result is
                jr      nz,.is_0_or_sc  ; CF = (A == 0 || A == ';')
                ; if space, the single space may be consumed in some conditions
                inc     de              ; "consume" the single space from input
                ld      a,(de)
                cp      ' '             ; two+ spaces is too much, don't "consume" it
                jr      z,.no_implicit_sp
                call    .is_0_or_sc
                ccf
                ret     c               ; if (space and !(null || ';')) => CF=1 and ++DE
.no_implicit_sp:                        ; return CF=1 (is instruction)
                dec     de              ; but don't consume the [implicit] space
                scf
                ret
.is_0_or_sc:                            ; CF = (A == 0 || A == ';')
                cp      ';'
                scf
                ret     z
                cp      1
                ret

;; check if following input char after token keyword is non-id char (success), or if last
;; char of token is non-id char (success), otherwise (id-char followed by id-char fail)
;; returns CF = 1 when success (instruction-token does match)
                dw      TokenTable      ; address of my token table (at -2 address of routine)
tokenise_is_op:
                call    isIdentChar
                ret     c               ; token is followed by non-id char, report success
                ld      a,(hl)
                jp      isIdentChar     ; check last char of token, if non-id, report success

;;----------------------------------------------------------------------------------------------------------------------
;; tokeniseLine
;;
;; "mode" in B: 0 check tokens, 1 in id-string, 2 in comment, '"' or "'" in such quotes
;;
;; Input:
;;      HL = Output buffer
;;      DE = Input buffer
;;
;; Output:
;;      HL = end of output buffer (points to null terminator)
;;      DE = end of input buffer (points to null terminator)
;;      B = OK: 0|1|2 (1 when ends with id-string, 2 when ends in comment)
;;          BAD: single/double quote char => missing closing quote
;;      C = number of spaces at end of line
;;
;; Affects:
;;      AF, IX
;;

tokeniseLine:
                ld      ix,tokenise_is_ins  ; search for instruction token first
                dec     hl              ; patch --HL, --DE to pass through .accept_mode
                dec     de              ; (to read first character of input and set B,C)
                xor     a               ; reset mode to 0 (will become B)
.accept_mode:
                ld      b,a             ; accept char and char also becomes new mode
.accept:
                ld      c,0             ; reset space counter in C
                inc     hl
.no_accept:
                inc     de
                ld      a,(de)
                ld      (hl),a          ; Copy character
                or      a               ; Reached end?
                ret     z

                ; Check first if inside id-string, before processing space (space ends id)

                djnz    .not_id_string
                call    isIdentChar     ; Inside id-string, keep accepting any id-char
                jr      c,.id_did_end   ; No, process char in B=0 mode
                inc     b               ; restore B=1 and continue with id-string
                jr      .accept
.not_id_string:
                inc     b               ; restore B (modified by djnz)
.id_did_end:

                ; Process space characters (they get compresed anywhere in the line)

                cp      ' '             ; Was character a space?
                jr      nz,.not_space
                inc     c               ; Count the spaces
                jr      .no_accept      ; But don't accept it yet

.not_space:
                ; C = number of spaces seen before now
                ; Here we insert either a $0b..$20 compressed-space char (single to 22 spaces)
                ; or the sequence $0a, <num spaces> (23+ spaces)

                xor     a
                sub     c               ; A = -(number of spaces)
                jr      z,.no_spaces    ; no space at all, nothing to insert
                ld      (hl),$0a        ; prepare for [$0a,count] sequence alternative
                add     a,' '+1         ; create $0b to $20 for 22..1 spaces
                jr      nc,.use_0a_seq  ; result is not in $0b..$20 range, use $0a
                cp      $0b
                jr      nc,.use_0b_20   ; result *is* in $0b..$20 range, use single byte code
.use_0a_seq:
                inc     hl              ; confirm $0a in output
                ld      a,c             ; store unpatched count afterward
.use_0b_20:
                ldi     (hl),a
.no_spaces:

                ld      a,(de)
                ld      (hl),a          ; Copy character (after any space tokens)

                ; Check to see if we're in other special mode (";" comment or inside quotes)

                inc     b               ; check if B == 0
                djnz    .waiting_for_delimiter

                ; Check some special characters switching mode [;"'$]
                inc     b               ; B = 1 in case the hexa number literal follows
                cp      '$'
                jr      z,.accept
                inc     b               ; B = 2 in case the ";" comment starts here
                cp      ';'
                jr      z,.accept
                cp      $22
                jr      z,.accept_mode  ; double-quote starts here, B = double quote
                cp      "'"
                jr      z,.accept_mode  ; single-quote starts here, B = single quote

                ; This can be token, check the input against current table (by IX)
                push    hl
                call    tokenise
                pop     hl
                jr      c,.token_failed

                ; We've found a token in the input stream
                ld      ix,tokenise_is_op   ; switch to operand-tokens for next try
                dec     de              ; adjust --DE for ".accept" code-path
                ld      (hl),b
                xor     a
                cp      c               ; Is token sub-index == 0?
                jr      z,.accept_mode  ; Yes, tokenise rest of line
                inc     hl
                ld      (hl),c          ; Otherwise, write sub-index too
                ld      c,a
                jr      .accept_mode

                ; Token not found, accept the character "as is", but start id-string if needed
.token_failed:
                ld      a,(de)
                ld      b,$01           ; B = 1 (id-string)
                call    isIdentChar
                jr      nc,.accept

                ; not id-char, just accept it with B = 0
                dec     b
                jr      .accept

.waiting_for_delimiter:
                xor     b               ; compare character with expected end-delimiter
                jr      nz,.accept      ; didn't match, keep accepting any chars inside
                jr      .accept_mode    ; accept the ending delimiter, set B=0

;;----------------------------------------------------------------------------------------------------------------------
;; detokenise
;; Detokenise a line into a buffer.  To detokenise with 2nd table, set C to 0 and call detokenise.entry.
;;
;; Input:
;;      HL = input buffer
;;      DE = output buffer (256-byte aligned)
;;
;; Output:
;;      HL = Input buffer after consumed input (points to null terminator)
;;      DE = New position within input buffer after token (points to null terminator)
;;
;; Affects:
;;      AF, BC
;;

detokenise:
                ld      c,1             ; counter to use first token table vs second
                jr      .entry
.accept:    ; when value written at (de) is valid output, advance DE and input
                inc     e
                jr      nz,.next
.overflow:
                dec     e               ; stay at $xxFF until null in input
.next:      ; when DE is already pointing where next output goes, advance input
                inc     hl
.entry:
                ld      a,(hl)
.entry_a:
                ld      (de),a
                or      a
                ret     z               ; reached end of input buffer ($00 input)
                jp      m,.token        ; $80+ is token id
                sub     $21
                jr      nc,.accept      ; $21..$7f are regular characters, just confirm it
            ; input $01 .. $20 transformed into -32 .. -1
                neg                     ; $20 -> 1, $1f -> 2, ... $0b -> 22, $0a -> 23
                cp      23
                jr      c,.unpack       ; 1..22 from $0b..$20 is count of spaces to insert
                jr      nz,.next        ; 24+ is invalid input ($01..$09), ignore it
                ; $0a has in next byte count of spaces to insert
                inc     hl
                ld      a,(hl)          ; $0a reads the count from input stream
.unpack:
                ld      b,a
                ld      a,' '
.l1:
                ld      (de),a
                inc     e
                jr      z,.overflow
                djnz    .l1
                jr      .next
.token:
                push    bc
                ld      b,a             ; main token index
                inc     hl
                ld      a,(hl)
                cp      3               ; 1..2 is sub-index (null terminator as 0 sub-index = ok)
                jr      c,.sub_index
                xor     a               ; no sub-index, use zero
.sub_index:
                or      a               ; if non-zero sub-index, advance HL beyond it
                jr      z,.zero_sub
                inc     hl
.zero_sub:
                push    hl
                ld      hl,TokenTable   ; second table for operand tokens
                dec     c
                jr      nz,.operand_token
                ld      hl,InsTokenTable; first table for instruction tokens
.operand_token:
                ld      c,a             ; sub-index finally loaded into C
                call    findToken
                call    nc,.found       ; copy token text if found
                pop     hl              ; HL points to next input, DE to next write
                pop     bc
                dec     c
                jr      nz,.entry       ; operand token written,
                ; instruction-token, add implicit space if following input is not (null|;|space)
                ld      a,(hl)
                cp      $21
                jr      c,.entry_a      ; null-to-space input follows: continue with it
                cp      ';'             ; following semicolon also cancels implicit space
                jr      z,.entry_a      ; just store the semicolon and continue
                dec     hl              ; HL will be re-advanced after writing space
                ld      a,' '           ; add implicit space
                jr      .entry_a
.found:
                ldi     a,(hl)
                or      a
                ret     m               ; token text did end
                ld      (de),a
                inc     e
                jr      nz,.found       ; accept char + loop if not overflow
                dec     e
                ret                     ; emergency exit in case of overflow

;;----------------------------------------------------------------------------------------------------------------------
;; findToken
;;
;; Input:
;;      HL = token table (each token must have at least one character of text)
;;      B = token index
;;      C = token sub-index
;;
;; Output:
;;      HL = nth token in table (beginning of token text)
;;      CF = 1 if not found (HL points beyond the token table)
;;
;; Affects:
;;      AF
;;

findToken:
                ldi     a,(hl)
                cp      b
                jr      nz,.test_end
                ldi     a,(hl)                  ; Grab the sub-token
                cp      c
                ret     z                       ; HL points at the token text, CF=0
                ; continue through .test_end, A is sub-index 0..3, so it will not end search
                ; also HL already point at first char, so `bit 7,(hl)` will test from 2nd+ position
.test_end:
                add     a,1                     ; End of list? ($FF OP_UNKNOWN is marker)
                ret     z                       ; ZF = 1 and CF = 1 if not found (end of table reached)
.loop:
                inc     hl                      ; Skip past the text to beginning of next table item
                bit     7,(hl)
                jr      z,.loop                 ; Find end of token entry
                jr      findToken

;;----------------------------------------------------------------------------------------------------------------------
;; Opcode/Directive token table
;;
;; Matching and detokenise rules - first table (instruction-tokens):
;;   - to match text the following character on line must be space, semicolon or null terminator
;;   - if there is only single space not followed by semicolon or null, the space is considered/consumed as part of token
;;   - to detokenise - implicit space is added after each instruction, if null/semicolon/space does not follow
;;   - tokens must be sorted by first letter (only first, second+ can be in any order, so "AF'" can be found before "AF")
;;

OP_ADC          equ     $80
OP_ADD          equ     $81
OP_AND          equ     $82
OP_BIT          equ     $83
OP_CALL         equ     $84
OP_CCF          equ     $85
OP_CP           equ     $86
OP_CPD          equ     $87
OP_CPDR         equ     $88
OP_CPI          equ     $89
OP_CPIR         equ     $8a
OP_CPL          equ     $8b
OP_DAA          equ     $8c
OP_DEC          equ     $8d
OP_DEFB         equ     $8e
OP_DEFC         equ     $e3
OP_DEFZ         equ     $8f
OP_DEFS         equ     $90
OP_DEFW         equ     $91
OP_DI           equ     $92
OP_DJNZ         equ     $94
OP_EI           equ     $95
OP_ENT          equ     $96
OP_EQU          equ     $97
OP_EX           equ     $98
OP_EXA          equ     $e4
OP_EXX          equ     $99
OP_HALT         equ     $9a
OP_IM           equ     $9b
OP_IN           equ     $9c
OP_INC          equ     $9d
OP_IND          equ     $9e
OP_INDR         equ     $9f
OP_INI          equ     $a0
OP_INIR         equ     $a1
OP_JP           equ     $a2
OP_JR           equ     $a3
OP_LD           equ     $a4
OP_LDD          equ     $a5
OP_LDDR         equ     $a6
OP_LDI          equ     $a7
OP_LDIR         equ     $a8
OP_NEG          equ     $a9
OP_NOP          equ     $aa
OP_OPT          equ     $93
OP_OR           equ     $ab
OP_ORG          equ     $ac
OP_OTDR         equ     $ad
OP_OTIR         equ     $ae
OP_OUT          equ     $af
OP_OUTD         equ     $b0
OP_OUTI         equ     $b1
OP_POP          equ     $b2
OP_PUSH         equ     $b3
OP_RES          equ     $b4
OP_RET          equ     $b5
OP_RETI         equ     $b6
OP_RETN         equ     $b7
OP_RL           equ     $b8
OP_RLA          equ     $b9
OP_RLC          equ     $ba
OP_RLCA         equ     $bb
OP_RLD          equ     $bc
OP_RR           equ     $bd
OP_RRA          equ     $be
OP_RRC          equ     $bf
OP_RRCA         equ     $c0
OP_RRD          equ     $c1
OP_RST          equ     $c2
OP_SBC          equ     $c3
OP_SCF          equ     $c4
OP_SET          equ     $c5
OP_SLA          equ     $c6
OP_SRA          equ     $c7
OP_SRL          equ     $c8
OP_SUB          equ     $c9
OP_XOR          equ     $ca
OP_SL1          equ     $cb
OP_SWAP         equ     $cc
OP_MIRR         equ     $cd
OP_TEST         equ     $ce
OP_BRLC         equ     $d3
OP_BSLA         equ     $cf
OP_BSRA         equ     $d0
OP_BSRL         equ     $d1
OP_BSRF         equ     $d2
OP_MUL          equ     $d4
OP_OTIB         equ     $d5
OP_NREG         equ     $d6
OP_PXDN         equ     $d7
OP_PXAD         equ     $d8
OP_STAE         equ     $d9
OP_LDIX         equ     $da
OP_LDWS         equ     $db
OP_LDDX         equ     $dc
OP_LIRX         equ     $dd
OP_LPRX         equ     $de
OP_LDRX         equ     $df
OP_INCBIN       equ     $e0
OP_INCLUDE      equ     $e1
OP_SAVE         equ     $e2
OP_TAB          equ     $e5
OP_ENDT         equ     $e6
OP_ENTB         equ     $e7
OP_ENTC         equ     $e8
OP_ENTS         equ     $e9
OP_ENTW         equ     $ea
OP_ENTZ         equ     $eb
OP_UNKNOWN      equ     $ff

InsTokenTable:
                db OP_ADC,$00,"ADC"
                db OP_ADD,$00,"ADD"
                db OP_AND,$00,"AND"
                db OP_INCBIN,$00,"BIN"
                db OP_BIT,$00,"BIT"
                db OP_BRLC,$00,"BRLC"
                db OP_BSLA,$00,"BSLA"
                db OP_BSRA,$00,"BSRA"
                db OP_BSRF,$00,"BSRF"
                db OP_BSRL,$00,"BSRL"
                db OP_CALL,$00,"CALL"
                db OP_CCF,$00,"CCF"
                db OP_CP,$00,"CP"
                db OP_CPD,$00,"CPD"
                db OP_CPDR,$00,"CPDR"
                db OP_CPI,$00,"CPI"
                db OP_CPIR,$00,"CPIR"
                db OP_CPL,$00,"CPL"
                db OP_DAA,$00,"DAA"
                db OP_DEFB,$00,"DB"
                db OP_DEFC,$00,"DC"
                db OP_DEC,$00,"DEC"
                db OP_DEFB,$01,"DEFB"
                db OP_DEFC,$01,"DEFC"
                db OP_DEFS,$01,"DEFS"
                db OP_DEFW,$01,"DEFW"
                db OP_DEFZ,$01,"DEFZ"
                db OP_DI,$00,"DI"
                db OP_DJNZ,$00,"DJNZ"
                db OP_DEFS,$00,"DS"
                db OP_DEFW,$00,"DW"
                db OP_DEFZ,$00,"DZ"
                db OP_EI,$00,"EI"
                db OP_ENDT,$00,"ENDT"
                db OP_ENT,$00,"ENT"
                db OP_ENTB,$00,"ENTB"
                db OP_ENTC,$00,"ENTC"
                db OP_ENTS,$00,"ENTS"
                db OP_ENTW,$00,"ENTW"
                db OP_ENTZ,$00,"ENTZ"
                db OP_EQU,$00,"EQU"
                db OP_EX,$00,"EX"
                db OP_EXA,$00,"EXA"
                db OP_EXX,$00,"EXX"
                db OP_HALT,$00,"HALT"
                db OP_IM,$00,"IM"
                db OP_IN,$00,"IN"
                db OP_INC,$00,"INC"
                db OP_INCBIN,$01,"INCBIN"
                db OP_INCLUDE,$01,"INCLUDE"
                db OP_IND,$00,"IND"
                db OP_INDR,$00,"INDR"
                db OP_INI,$00,"INI"
                db OP_INIR,$00,"INIR"
                db OP_JP,$00,"JP"
                db OP_JR,$00,"JR"
                db OP_LD,$00,"LD"
                db OP_LDD,$00,"LDD"
                db OP_LDDR,$00,"LDDR"
                db OP_LDRX,$01,"LDDRX"
                db OP_LDDX,$00,"LDDX"
                db OP_LDI,$00,"LDI"
                db OP_LDIR,$00,"LDIR"
                db OP_LIRX,$01,"LDIRX"
                db OP_LDIX,$00,"LDIX"
                db OP_LPRX,$01,"LDPIRX"
                db OP_LDRX,$00,"LDRX"
                db OP_LDWS,$00,"LDWS"
                db OP_LIRX,$00,"LIRX"
                db OP_INCLUDE,$00,"LOAD"
                db OP_LPRX,$00,"LPRX"
                db OP_MIRR,$00,"MIRR"
                db OP_MIRR,$01,"MIRROR"
                db OP_MUL,$00,"MUL"
                db OP_NEG,$00,"NEG"
                db OP_NREG,$01,"NEXTREG"
                db OP_NOP,$00,"NOP"
                db OP_NREG,$00,"NREG"
                db OP_OPT,$00,"OPT"
                db OP_OR,$00,"OR"
                db OP_ORG,$00,"ORG"
                db OP_OTDR,$00,"OTDR"
                db OP_OTIB,$00,"OTIB"
                db OP_OTIR,$00,"OTIR"
                db OP_OUT,$00,"OUT"
                db OP_OUTD,$00,"OUTD"
                db OP_OUTI,$00,"OUTI"
                db OP_OTIB,$01,"OUTINB"
                db OP_PXAD,$01,"PIXELAD"
                db OP_PXDN,$01,"PIXELDN"
                db OP_POP,$00,"POP"
                db OP_PUSH,$00,"PUSH"
                db OP_PXAD,$00,"PXAD"
                db OP_PXDN,$00,"PXDN"
                db OP_RES,$00,"RES"
                db OP_RET,$00,"RET"
                db OP_RETI,$00,"RETI"
                db OP_RETN,$00,"RETN"
                db OP_RL,$00,"RL"
                db OP_RLA,$00,"RLA"
                db OP_RLC,$00,"RLC"
                db OP_RLCA,$00,"RLCA"
                db OP_RLD,$00,"RLD"
                db OP_RR,$00,"RR"
                db OP_RRA,$00,"RRA"
                db OP_RRC,$00,"RRC"
                db OP_RRCA,$00,"RRCA"
                db OP_RRD,$00,"RRD"
                db OP_RST,$00,"RST"
                db OP_SAVE,$00,"SAVE"
                db OP_SBC,$00,"SBC"
                db OP_SCF,$00,"SCF"
                db OP_SET,$00,"SET"
                db OP_STAE,$01,"SETAE"
                db OP_SL1,$00,"SL1"
                db OP_SLA,$00,"SLA"
                db OP_SRA,$00,"SRA"
                db OP_SRL,$00,"SRL"
                db OP_STAE,$00,"STAE"
                db OP_SUB,$00,"SUB"
                db OP_SWAP,$00,"SWAP"
                db OP_SWAP,$01,"SWAPNIB"
                db OP_TAB,$00,"TAB"
                db OP_TEST,$00,"TEST"
                db OP_XOR,$00,"XOR"


                db OP_UNKNOWN


;;----------------------------------------------------------------------------------------------------------------------
;; Operands/Other token table
;;
;; Matching and detokenise rules - second table (operand-tokens):
;;   - to match text the token-text must match, and if it ends with id-character, the non-id char must follow
;;     examples: "<<<" -> does match "<<" + "<" left in input, "BD" does NOT match "B", "BCD" does NOT match "BC", but "B<" does match "B"
;;   - to detokenise - just token-text is added into output, without extra space or other delimiter
;;   - tokens must be sorted by first letter (only first, second+ can be in any order, so "AF'" can be found before "AF")
;;

; The order of these opcodes match the order when decoding opcode bits
OP_B            equ     $80
OP_C            equ     $81
OP_D            equ     $82
OP_E            equ     $83
OP_H            equ     $84
OP_L            equ     $85
OP_AFA          equ     $86
OP_A            equ     $87

OP_BC           equ     $88
OP_DE           equ     $89
OP_HL           equ     $8a
OP_SP           equ     $8b
OP_AF           equ     $8c

OP_I            equ     $8d
OP_IX           equ     $8e
OP_IY           equ     $8f
OP_M            equ     $90
OP_NC           equ     $91
OP_NZ           equ     $93
OP_P            equ     $94
OP_PE           equ     $95
OP_PO           equ     $96
OP_R            equ     $97
OP_Z            equ     $99

OP_SHL          equ     $9a
OP_SHR          equ     $9b
OP_MOD          equ     $9c

OP_IXH          equ     $9d
OP_IXL          equ     $9e
OP_IYH          equ     $9f
OP_IYL          equ     $a0

; WARNING: do not use for new expression operators (like MOD/SHL/SHR) values: $ab, $ad, $fe
; they would clash with ET_U_PLUS, ET_U_MINUS, ET_U_INVERT in expr.s

TokenTable:
                db OP_SHL,$00,"<<"
                db OP_SHR,$00,">>"
                db OP_A,$00,"A"
                db OP_AFA,$00,"AF'"     ; must be ahead of "AF" to find it correctly
                db OP_AF,$00,"AF"
                db OP_B,$00,"B"
                db OP_BC,$00,"BC"
                db OP_C,$00,"C"
                db OP_D,$00,"D"
                db OP_DE,$00,"DE"
                db OP_E,$00,"E"
                db OP_H,$00,"H"
                db OP_HL,$00,"HL"
                db OP_I,$00,"I"
                db OP_IX,$00,"IX"
                db OP_IXH,$01,"IXH"
                db OP_IXL,$01,"IXL"
                db OP_IY,$00,"IY"
                db OP_IYH,$01,"IYH"
                db OP_IYL,$01,"IYL"
                db OP_L,$00,"L"
                db OP_M,$00,"M"
                db OP_MOD,$00,"MOD"
                db OP_NC,$00,"NC"
                db OP_PO,$01,"NV"       ; new "NV" definition, being PO alias (Dev.2h was $92,$00)
                db OP_NZ,$00,"NZ"
                db OP_P,$00,"P"
                db OP_PE,$00,"PE"
                db OP_PO,$00,"PO"
                db OP_R,$00,"R"
                db OP_SP,$00,"SP"
                db OP_PE,$01,"V"        ; new "V" definition, being PE alias (Dev.2h was $98,$00)
                db OP_IXH,$00,"XH"
                db OP_IXL,$00,"XL"
                db OP_IYH,$00,"YH"
                db OP_IYL,$00,"YL"
                db OP_Z,$00,"Z"


                db OP_UNKNOWN

