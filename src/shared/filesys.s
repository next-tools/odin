;;----------------------------------------------------------------------------------------------------------------------
;; File System management
;;----------------------------------------------------------------------------------------------------------------------

FILE            macro   func

                call    pageDivMMC
                dos     func
                call    pageOutDivMMC

                endm

;;----------------------------------------------------------------------------------------------------------------------
;; Variables
;;

FileSysHandle   db      0               ; Current file handle created by last fOpen
FileSysStat     ds      11              ; Space to store file information

FileBufferLen   db      0               ; Amount of data read into file buffer

;;----------------------------------------------------------------------------------------------------------------------
;; fOpenFile
;; Open a file based on a given mode.
;;
;; Input:
;;      FileName buffer = name of file to open.
;;      B = open mode
;;
;; Output:
;;      CF = 1 if error occurs.
;;      HL = FileBuffer
;;

fOpenFile:
                push    af,bc,de,hl,ix

                xor     a
                FILE    M_GETSETDRV
                jr      c,fError

                ld      hl,FileName
                ld      de,.dosHeader
                push    hl
                pop     ix
                FILE    F_OPEN
                jr      c,fError
                ld      (FileSysHandle),a
                xor     a
                ld      (FileBufferLen),a
                pop     ix,hl
                ld      hl,FileBuffer           ; Set up HL for streamed read/write if that's what we want
                jr      fDoneHL

.dosHeader      ds      8

fDone:
                pop     ix
                pop     hl
fDoneHL:
                pop     de
fDoneHLDE:
                pop     bc,af
                and     a
                ret

fError:
                pop     ix,hl,de,bc,af
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fOpenLoad
;; Open a file, whose filename is stored in FileName buffer.  FileSysHandle will store the handle.
;;
;; Output:
;;      CF = 1 if error occurred
;;

fOpenLoad:
                push    bc
                ld      b,FA_READ | FA_OPEN_EXISTING | FA_RWP3HDR
                call    fOpenFile
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fOpenSave
;; Open a file for saving to.  The filename will be in the FileName buffer.  FileSysHandle will store the handle.
;;
;; Output:
;;      CF = 1 if error occurred
;;

fOpenSave:
                push    bc
                ld      b,FA_WRITE | FA_CREATE_NEW
                call    fOpenFile
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fOpenAppend
;; Open a file for saving to but keep the previous contents.  The filename will be in the FileName buffer.
;; FileSysHandle will store the handle.
;;
;; Output:
;;      CF = 1 if error occurred
;;

fOpenAppend:
                push    bc
                ld      b,FA_WRITE | FA_OPEN_EXISTING
                call    fOpenFile
                jr      c,.error

                ; Seek to the end
                call    fGetSize
                call    fSeek

.error:
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fGetSize
;; Get the size of the currently opened file.
;;
;; Output:
;;      DEHL = file size
;;

fGetSize:
                push    af,bc,de,hl,ix
                ld      a,(FileSysHandle)
                ld      hl,FileSysStat
                push    hl
                pop     ix
                FILE    F_FSTAT
                jr      c,fError

                pop     ix,hl,de
                ld      hl,(FileSysStat+7)
                ld      de,(FileSysStat+9)
                jr      fDoneHLDE

;;----------------------------------------------------------------------------------------------------------------------
;; fSeek
;; See to a new position in the file.
;;
;; Input:
;;      DEHL = file position to seek to
;;

fSeek:
                push    af,bc,de,hl,ix
                ld      a,(FileSysHandle)
                ld      bc,de
                ld      de,hl           ; BCDE = file position
                ld      l,0
                push    hl
                pop     ix
                FILE    F_SEEK
                jp      fDone

;;----------------------------------------------------------------------------------------------------------------------
;; fClose
;; Close the file we currently have opened
;;

fClose:
                ld      a,(FileSysHandle)
                FILE    F_CLOSE
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fRead
;; Read a chunk of data from a file.  Do not use in conjunction with fReadByte and this uses the buffering system.
;;
;; Input:
;;      HL = address to write data to
;;      BC = number of bytes to read
;;
;; Output:
;;      BC = number of bytes actually read
;;      CF = 1 if error occurs
;;

fRead:
                or      a               ; enforce CF=0 after `pop af`
                push    af,de,hl
                ld      a,(FileSysHandle)
                FILE    F_READ
                pop     hl,de
                jr      c,.error
                pop     af
                ret
.error:
                pop     af
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fWrite
;; Read a chunk of data to a file.  Do not use in conjunction with fWriteByte and this uses the buffering system.
;;
;; Input:
;;      HL = address to write data from
;;      BC = number of bytes to read
;;
;; Output:
;;      CF = 1 if error occurs
;;

fWrite:
                push    af,bc,de,hl,ix
                ld      a,(FileSysHandle)
                push    hl
                pop     ix
                FILE    F_WRITE
                jp      c,fError
                jp      fDone

;;----------------------------------------------------------------------------------------------------------------------
;; fReadByte
;; Read a single byte from the currently opened file.
;;
;; Input:
;;      HL = position in file buffer
;;
;; Output:
;;      A = byte
;;      HL = next position in file buffer
;;      CF = 1 if error occurred
;;      ZF = 1 if no more data
;;

fReadByte:
                ld      a,(FileBufferLen)
                cp      l                       ; also enforces CF = 0 for buffer read
                jr      nz,.read

                ; no data, try to read 255 more
                push    bc
                ld      bc,255                  ; Read in 255 bytes
                ld      l,b                     ; HL = FileBuffer (it's 256B aligned)
                call    fRead
                ld      a,c
                ld      (FileBufferLen),a       ; Write number of bytes left
                pop     bc
                ret     c                       ; CF=1 report error!
                and     a                       ; Any bytes read?
                ret     z                       ; No - no more data!

.read:
                ld      a,(hl)
                inc     l                       ; ZF = 0
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fWriteByte
;; Write a single byte to the currently opened file.
;;
;; NOTE: Do not forget to call fFlush before fClose!
;;
;; Input:
;;      HL = position in buffer
;;      A = byte to write
;;
;; Output:
;;      HL = next position in file buffer to write
;;      CF = 1 if error occurs
;;

;; currently unused - if needed, uncomment (+ re-test thoroughly, especially edge-case like fFlush on empty buffer)
; fWriteByte:
;                 or      a               ; CF = 0 for non-write call
;                 ld      (hl),a
;                 inc     l
;                 ret     nz
;                 push    bc
;                 ld      bc,256
; .write:
;                 ld      hl,FileBuffer
;                 call    fWrite
;                 pop     bc
;                 ret

;;----------------------------------------------------------------------------------------------------------------------
;; fFlush
;; Write all bytes still in the file buffer to the file
;;
;; Input:
;;      L = number of bytes to write.
;;
;; Output:
;;      HL = next position in file buffer to write
;;      CF = 1 if error occurs
;;

; fFlush:
;                 push    bc
;                 ld      c,l
;                 ld      b,0
;                 jr      fWriteByte.write

;;----------------------------------------------------------------------------------------------------------------------
;; fGetCwd
;; Get current folder into FileName buffer
;;
;; Output:
;;      CF = 1 if error occurs
;;

fGetCwd:
                push    af,hl

                ld      a,'*'
                ld      hl,FileName
                FILE    F_GETCWD

.end:
                pop     hl,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fNormalise
;; Normalises a filename to make it a full absolute filename.  This will handle absolute and relative paths as well
;; as . and ..
;;
;; Input:
;;      HL = start of filename
;;      DE = end of filename string
;;
;; Output:
;;      FileName = full filename
;;      BC = end of filename: (BC) == 0
;;      CF = 1 if filename is invalid (too long or incorrect)
;;
;;
;; First we determine what kind of filename it is:
;;
;;      Example         Type                Prefix      Normalise       Notes
;;
;;      C:/foo/bar      Global Absolute     C:/         foo/bar
;;      C:foo/bar       Drive Relative      C:/<cd>     foo/bar         <cd> current directory in given drive
;;      /foo/bar        Local Absolute      X:/         foo/bar         X = current drive
;;      foo/bar         Local Relative      <cd>/       foo/bar         <cd> current directory and drive
;;

fNormalise:
                push    de,hl
                ld      bc,FileName             ; BC points to filename

                call    compare16
                scf
                ret     z                       ; Filename is empty!

                ;;
                ;; Step 1 - Determine the type
                ;;

                ; If we start with a /, we are an absolute path
                call    fGetNextChar
                ld      (.drive),a              ; Store possible drive letter
                scf
                ret     z                       ; String is empty
                cp      '/'
                jr      z,.localAbs

                ; Check drive letter
                call    isAlpha
                jr      c,.localRel_1           ; Not an alpha character so definitely a local relative path
                call    fGetNextChar
                jr      z,.localRel_1           ; Single lettered filename - local relative path
                cp      ':'
                jr      nz,.localRel_2          ; Not a drive letter starting so definitely a local relative path too
                call    fGetNextChar
                jr      z,.error                ; Filename is just "X:" - an invalid path
                cp      '/'
                jr      z,.globalAbs            ; Filename starts with "C:/" so global absolute path

                ;;
                ;; Step 2 - Stamp the initial prefix
                ;;

.driveRel:
                ; HL = 2nd character normalisation
                dec     hl
                ld      a,(.drive)
                call    .set_drive_spec
                jr      nc,.normalise
.error:
                scf
                pop     hl,de
                ret

.drive:         db      0
.cwd            db      0                       ; 1 = get current directory from assembler

.set_drive_spec:
                call    upperCase
                sub     $41
                add     a,a
                add     a,a
                add     a,a
                inc     a

.stamp_cd:
                ; Output: BC = end of prefix, (BC) == 0, FileName contains current directory
                push    de,hl,af
                ld      a,(.cwd)               ; 0 = external current directory
                and     a
                jr      nz,.asm_cd
                pop     af
                
                ld      hl,FileName
                FILE    F_GETCWD                ; FileName = "X:/cd/"
                jr      c,.stamp_done
                ld      hl,FileName
                call    strEnd
                ld      bc,hl
.stamp_done:
                pop     hl,de
                ret
.asm_cd:
                pop     af
                call    asmGetCurrentDirectory
                jr      .stamp_done

.localRel_2:
                ; HL = 2nd or 3rd character
                dec     hl
.localRel_1:
                dec     hl
                ld      a,'*'
                call    .stamp_cd
                jr      c,.error
                jr      .normalise

.globalAbs:
                ; HL = start of normalisation
                ld      a,(.drive)
                call    upperCase
                ldi     (bc),a                  ; FileName = "X"
                ld      a,':'
                ldi     (bc),a                  ; FileName = "X:"
                ld      a,'/'
                ldi     (bc),a                  ; FileName = "X:/"
                jr      .normalise

.localAbs:
                ; HL = start of normalisation
                ld      a,'*'
                call    .stamp_cd
                jr      c,.error
                ld      bc,FileName+3

.normalise:
                ;;
                ;; Step 3 - Go through each path and append it
                ;;
                ;; BC = write position
                ;; HL = read position
                ;; DE = end of read buffer

                call    fGetNextChar            ; A = next character
                jr      z,.term                 ; We're finished
                cp      '.'                     ; Are we . or ..?
                jr      nz,.path_1              ; No, so handle normally

                call    fGetNextChar
                jr      z,.error                ; Path ends in /. so error!
                cp      '/'
                jr      z,.normalise            ; . found - just ignore
                cp      '.'
                jr      nz,.path_2              ; Not .. - so just handle normally

                call    fGetNextChar
                jr      z,.parent
                cp      '/'
                jr      nz,.path_3

.parent:
                ; Erase the previous path
                dec     bc                      ; Skip trailing /
                call    fParent
                jr      .normalise

.path_3:        dec     hl
.path_2:        dec     hl
.path_1:        dec     hl

                ; HL = path terminated by '/' or end of buffer
.next_char:
                call    fGetNextChar
                jr      z,.term

                ldi     (bc),a
                cp      '/'
                jr      z,.normalise
                jr      .next_char

.term:
                xor     a
                ld      (bc),a
.done:
                pop     hl,de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fParent
;; BC points to end of filename, point to end of previous path
;;
;; Output:
;;      CF = 1 means there are no parents.  Then BC points to ':'
;;

fParent:
                dec     bc
                ld      a,(bc)
                cp      ':'
                scf
                ret     z
                cp      '/'
                jr      nz,fParent
.end
                inc     bc                      ; Point after the /
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fGetNextChar
;; Given a buffer defined by HL->BC, get the next character in A and advance BC.  If we have no characters, ZF=1.
;;

fGetNextChar:
                call    compare16
                ret     z               ; No more characters
                ld      a,(hl)
                inc     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fEnsureOdn
;; Ensure that the filename in the FileName buffer has the extension odn on.  Filename must be a fully normalised name.
;;
;; Output:
;;      CF = 1 not enough room to add extension.
;;      CF = 0, ZF = 1 already has an extension.
;;      CF = 0, ZF = 0 extension added.
;;

odnExt          dz      ".odn"

fEnsureOdn:
                push    hl,de
                ld      hl,FileName
                call    strEnd

.prev:
                dec     hl
                ld      a,(hl)
                cp      '.'
                jr      z,.end          ; CF = 0, ZF = 1: already has extension
                cp      '/'
                jr      nz,.prev

                ; We've found the last element of the path and it has no extension
                call    strEnd
                
                ld      de,EndFileName-5
                call    compare16
                ccf
                jr      c,.end          ; Not enough room
                
                ld      de,odnExt
                push    bc
                ld      bc,5
                ex      de,hl
                ldir                    ; Add extension
                pop     bc
                
                res_zf                  ; Clear ZF and CF

.end:
                pop     de,hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

