;;----------------------------------------------------------------------------------------------------------------------
;; getArg
;; Obtain the next argument and initialise a null terminated buffer with that argument.
;;
;; Input:
;;      HL = command line
;;      DE = destination buffer for next argument (256 bytes max)
;; Output:
;;      CF = 0: no argument, 1: argument found
;;      HL = command line following this argument
;;      BC = length of argument (1..255)
;;      DE = Byte after null terminator in buffer
;;

getArg:
                push    de
                call    .internal
                pop     de
                ret

.internal
                ld      bc,0            ; Initialise size to 0
                ld      a,h
                or      l
                ret     z               ; No arguments
.l1:
                ld      a,(hl)          ; Fetch next character from command line
                inc     hl
                and     a               ; $00 found?
                ret     z               ; We're done here!
                cp      $0d             ; Newline?
                ret     z               ; Also, done here!
                cp      ':'             ; Colon?
                ret     z               ; This also finishes.
                cp      ' '
                jr      z,.l1           ; Skip spaces
                cp      $22             ; Now let's handle quotes
                jr      z,.quoted

.unquoted:
                ld      (de),a          ; Actual character we want to store
                inc     de
                inc     c               ; Increment length (maximum will be 255)
                jr      z,.bad_size     ; Don't allow >255

                ld      a,(hl)
                and     a
                jr      z,.complete
                cp      $0d
                jr      z,.complete
                cp      ':'
                jr      z,.complete
                cp      $22             ; This quote indicates next arg
                jr      z,.complete

                inc     hl
                cp      ' '
                jr      nz,.unquoted

.complete:
                xor     a
                ld      (de),a          ; terminate the string

                scf                     ; Found argument
                ret

.quoted:
                ld      a,(hl)
                and     a
                jr      z,.complete
                cp      $0d
                jr      z,.complete
                inc     hl
                cp      $22
                jr      z,.complete     ; Found matching quote

                ld      (de),a
                inc     de
                inc     c
                jr      z,.bad_size     ; Don't allow >255
                jr      .quoted

.bad_size:
                and     a
                ret

