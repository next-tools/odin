;;----------------------------------------------------------------------------------------------------------------------
; document.s tests
; coverage: docNextLine, docPrevLine, docInsertSpace, docRemoveSpace, docReplaceLine, docNewLine, docNewLineAbove
;           docDeleteLine, docInsertLine

__test_document_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Document: "
                ; shared module must be already paged in (otherwise colouredPrint used above did crash)

                ;--------------------------------------------------------------------------------------------
                ; TEST docNextLine
                call    __t_doc_setupSet1   ; fake document pages for "set 1" of documents
                __T_SETUP 'n'
                ld      hl,__t_doc_doc1+kDocStart
                call    docNextLine
                ld      a,__t_default_a
                call    pageVideo
                __T_CHECK cf_hl : dw __t_doc_doc1+kDocStart
                __T_SETUP '2'
                ld      hl,__t_doc_doc2+kDocStart
                call    docNextLine
                ld      a,__t_default_a
                call    pageVideo
                __T_CHECK ncf_hl : dw __t_doc_doc2+kDocStart+1
                __T_SETUP '.'
                ld      hl,__t_doc_doc2+kDocStart+1
                call    docNextLine
                ld      a,__t_default_a
                call    pageVideo
                __T_CHECK cf_hl : dw __t_doc_doc2+kDocStart+1
                __T_SETUP '3'
                ld      hl,__t_doc_doc3+kDocStart
                call    docNextLine
                ld      a,__t_default_a
                call    pageVideo
                __T_CHECK ncf_hl : dw __t_doc_doc3+kDocStart+3

                ;--------------------------------------------------------------------------------------------
                ; TEST docPrevLine
                __T_SETUP 'p'
                ld      hl,__t_doc_doc1+kDocStart
                call    docPrevLine
                ld      a,__t_default_a
                call    pageVideo
                __T_CHECK cf_hl : dw __t_doc_doc1+kDocStart
                __T_SETUP '2'
                ld      hl,__t_doc_doc2+kDocStart+1
                call    docPrevLine
                ld      a,__t_default_a
                call    pageVideo
                __T_CHECK ncf_hl : dw __t_doc_doc2+kDocStart
                __T_SETUP '.'
                ld      hl,__t_doc_doc2+kDocStart
                call    docPrevLine
                ld      a,__t_default_a
                call    pageVideo
                __T_CHECK cf_hl : dw __t_doc_doc2+kDocStart
                __T_SETUP '3'
                ld      hl,__t_doc_doc3+kDocStart+3
                call    docPrevLine
                ld      a,__t_default_a
                call    pageVideo
                __T_CHECK ncf_hl : dw __t_doc_doc3+kDocStart

                ;--------------------------------------------------------------------------------------------
                ; TEST docInsertSpace
                xor     a
                ld      (MainDoc.Flags),a
                ld      hl,__t_doc_doc1.e
                ld      (MainDoc.Length),hl
                __T_SETUP 'i'
                ld      hl,__t_doc_doc1+kDocStart
                ld      bc,$c000-__t_doc_doc1.e ; should fail because of too many bytes
                call    docInsertSpace
                call    pageVideo
                ld      a,__t_default_a
                __T_MEMCMP __t_doc_doc1, <"ODS",0,-1,0,0,-1,$CC,$CC>
                __T_BYTECMP MainDoc.Flags, 0
                __T_WORDCMP MainDoc.Length, __t_doc_doc1.e
                __T_CHECK cf_hl_bc : dw __t_doc_doc1+kDocStart, $c000-__t_doc_doc1.e

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '2'
                ld      hl,__t_doc_doc1+kDocStart
                ld      bc,2
                call    docInsertSpace
                call    pageVideo
                ld      a,__t_default_a
                __T_MEMCMP __t_doc_doc1, <"ODS",0,-1,0,0,-1,0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc1.e+2
                __T_CHECK ncf_hl_bc : dw __t_doc_doc1+kDocStart, 2

                ;--------------------------------------------------------------------------------------------
                ; TEST docRemoveSpace
                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP 'r'
                ld      hl,__t_doc_doc1+kDocStart+1
                ld      bc,1
                call    docRemoveSpace
                call    pageVideo
                ld      a,__t_default_a
                __T_MEMCMP __t_doc_doc1, <"ODS",0,-1,0,0,0,-1,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc1.e+1
                __T_CHECK ncf_hl_bc : dw __t_doc_doc1+kDocStart+1, 1
                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '2'
                ld      hl,__t_doc_doc1+kDocStart+1
                ld      bc,3
                call    docRemoveSpace
                call    pageVideo
                ld      a,__t_default_a
                __T_MEMCMP __t_doc_doc1, <"ODS",0,-1,0,0,0,-1,-1>
                __T_BYTECMP MainDoc.Flags, 0
                __T_WORDCMP MainDoc.Length, __t_doc_doc1.e+1
                __T_CHECK cf_hl_bc : dw __t_doc_doc1+kDocStart+1, 3

                ;--------------------------------------------------------------------------------------------
                ; TEST docReplaceLine
                xor     a
                ld      (MainDoc.Flags),a
                ; technically doc4 includes doc1..doc3
                ; (test quirk to save work around pages setup, works for current docReplaceLine implementation)
                ld      hl,__t_doc_doc4.e
                ld      (MainDoc.Length),hl
                __T_SETUP 'R'           ; 0 bytes replacing with 2 bytes
                ld      hl,__t_doc_doc4+kDocStart
                ld      de,__t_doc_tk1
                ld      bc,__t_doc_tk1.l
                call    docReplaceLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,$19,OP_RET,0,-1,$CC>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e+2
                __T_CHECK only_ncf_de_hl_bc : dw __t_doc_tk1, __t_doc_doc4+kDocStart, __t_doc_tk1.l

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '2'           ; 2 bytes replacing with 2 bytes
                ld      hl,__t_doc_doc4+kDocStart
                ld      de,__t_doc_tk2
                ld      bc,__t_doc_tk2.l
                call    docReplaceLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,$12,OP_DAA,0,-1,$CC>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e+2
                __T_CHECK only_ncf_de_hl_bc : dw __t_doc_tk2, __t_doc_doc4+kDocStart, __t_doc_tk2.l

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'           ; 2 bytes replacing with **SAME** 2 bytes (should NOT dirty the doc)
                ld      hl,__t_doc_doc4+kDocStart
                ld      de,__t_doc_tk2
                ld      bc,__t_doc_tk2.l
                call    docReplaceLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,$12,OP_DAA,0,-1,$CC>
                __T_BYTECMP MainDoc.Flags, 0
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e+2
                __T_CHECK only_ncf_de_hl_bc : dw __t_doc_tk2, __t_doc_doc4+kDocStart, __t_doc_tk2.l

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '3'           ; 2 bytes replacing with 0 bytes
                ld      hl,__t_doc_doc4+kDocStart
                ld      de,__t_doc_tk1
                ld      bc,0
                call    docReplaceLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e
                __T_CHECK only_ncf_de_hl_bc : dw __t_doc_tk1, __t_doc_doc4+kDocStart, 0

                xor     a
                ld      (MainDoc.Flags),a
                ld      hl,__t_doc_doc4.e
                ld      (MainDoc.Length),hl
                __T_SETUP '4'           ; 0 bytes replacing with 1 byte
                ld      hl,__t_doc_doc4+kDocStart
                ld      de,__t_doc_tk1+1
                ld      bc,__t_doc_tk1.l-1
                call    docReplaceLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,OP_RET,0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e+1
                __T_CHECK only_ncf_de_hl_bc : dw __t_doc_tk1+1, __t_doc_doc4+kDocStart, __t_doc_tk1.l-1

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '5'           ; 1 byte replacing with 1 byte
                ld      hl,__t_doc_doc4+kDocStart
                ld      de,__t_doc_tk2+1
                ld      bc,__t_doc_tk2.l-1
                call    docReplaceLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,OP_DAA,0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e+1
                __T_CHECK only_ncf_de_hl_bc : dw __t_doc_tk2+1, __t_doc_doc4+kDocStart, __t_doc_tk2.l-1

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '6'           ; 1 bytes replacing with 0 bytes
                ld      hl,__t_doc_doc4+kDocStart
                ld      de,__t_doc_tk1
                ld      bc,0
                call    docReplaceLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e
                __T_CHECK only_ncf_de_hl_bc : dw __t_doc_tk1, __t_doc_doc4+kDocStart, 0

                ;--------------------------------------------------------------------------------------------
                ; TEST docNewLine
                xor     a
                ld      (MainDoc.Flags),a
                ; assuming MainDoc.Length == __t_doc_doc4.e from last docReplaceLine test
                __T_SETUP 'N'
                ld      hl,__t_doc_doc4+kDocStart
                call    docNewLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,0,0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e+1
                ld      a,__t_default_a
                __T_CHECK ncf_hl : dw __t_doc_doc4+kDocStart+1

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'
                ld      hl,__t_doc_doc4+kDocStart
                call    docNewLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,0,0,0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e+2
                ld      a,__t_default_a
                __T_CHECK ncf_hl : dw __t_doc_doc4+kDocStart+1

                xor     a
                ld      (MainDoc.Flags),a
                ld      hl,$BFFF
                ld      (MainDoc.Length),hl
                __T_SETUP '2'
                ld      hl,__t_doc_doc4+kDocStart
                call    docNewLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,0,0,0,-1>
                __T_BYTECMP MainDoc.Flags, 0
                __T_WORDCMP MainDoc.Length, $BFFF
                ld      a,__t_default_a
                ld      hl,__t_default_hl
                __T_CHECK cf

                ;--------------------------------------------------------------------------------------------
                ; TEST docNewLineAbove
                ; assuming MainDoc.Length == $BFFF and Flags == 0 from last docNewLine test
                __T_SETUP 'A'
                ld      hl,__t_doc_doc4+kDocStart
                call    docNewLineAbove
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,0,0,0,-1>
                __T_BYTECMP MainDoc.Flags, 0
                __T_WORDCMP MainDoc.Length, $BFFF
                ld      a,__t_default_a
                ld      hl,__t_default_hl
                __T_CHECK cf

                ld      hl,__t_doc_doc4.e+2
                ld      (MainDoc.Length),hl
                __T_SETUP '2'
                ld      hl,__t_doc_doc4+kDocStart   ; above first line in document
                call    docNewLineAbove
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,0,0,0,0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e+3
                ld      a,__t_default_a
                __T_CHECK ncf_hl : dw __t_doc_doc4+kDocStart

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '3'
                ld      hl,__t_doc_doc4+kDocStart+1   ; above second line in document
                call    docNewLineAbove
                call    pageVideo
                __T_MEMCMP __t_doc_doc4, <"ODS",0,-1,0,0,0,0,0,0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc4.e+4
                ld      a,__t_default_a
                __T_CHECK ncf_hl : dw __t_doc_doc4+kDocStart+1

                ;--------------------------------------------------------------------------------------------
                ; TEST docDeleteLine - depends on implementation of docPrevLine - must work also on document not starting at $0000

                xor     a
                ld      (MainDoc.Flags),a
                ; technically doc5 includes doc1..doc4 in length, but has $FF start marker confining this test
                ; (test quirk to save work around pages setup, works for current docDeleteLine implementation)
                ld      hl,__t_doc_doc5.e
                ld      (MainDoc.Length),hl
                ; delete all three lines, starting from second (to check all variants)
                __T_SETUP 'd'
                ld      hl,__t_doc_doc5+kDocStart+2
                call    docDeleteLine           ; regular line delete, HL just points on last line
                call    pageVideo
                __T_MEMCMP __t_doc_doc5, <"ODS",0,-1,0,OP_RET, 0, OP_RRA, 0, -1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc5.e-2
                ld      a,__t_default_a
                __T_CHECK ncf_hl : dw __t_doc_doc5+kDocStart+2

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '2'
                ld      hl,__t_doc_doc5+kDocStart+2
                call    docDeleteLine           ; delete last (second) line, HL should move to first, CF=1
                call    pageVideo
                __T_MEMCMP __t_doc_doc5, <"ODS",0,-1,0,OP_RET, 0, -1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc5.e-4
                ld      a,__t_default_a
                __T_CHECK cf_hl : dw __t_doc_doc5+kDocStart

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '3'
                ld      hl,__t_doc_doc5+kDocStart
                call    docDeleteLine           ; delete last remaining line (empty line should be inserted to keep doc valid)
                call    pageVideo
                __T_MEMCMP __t_doc_doc5, <"ODS",0,-1,0, 0, -1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc5.e-5
                ld      a,__t_default_a
                __T_CHECK ncf_hl : dw __t_doc_doc5+kDocStart

                ;--------------------------------------------------------------------------------------------
                ; TEST docInsertLine
                ; assumes __t_doc_doc5 being "empty line" from docDeleteLine tests
                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP 'I'
                ld      hl,__t_doc_doc5+kDocStart
                ld      de,__t_doc_tk2
                call    docInsertLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc5, <"ODS",0,-1,0, $12,OP_DAA,0, 0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc5.e-2
                ld      a,__t_default_a
                __T_CHECK ncf_de_hl : dw __t_doc_tk2, __t_doc_doc5+kDocStart

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'
                ld      hl,__t_doc_doc5+kDocStart
                ld      de,__t_doc_tk2
                call    docInsertLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc5, <"ODS",0,-1,0, $12,OP_DAA,0, $12,OP_DAA,0, 0,-1>
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP MainDoc.Length, __t_doc_doc5.e+1
                ld      a,__t_default_a
                __T_CHECK ncf_de_hl : dw __t_doc_tk2, __t_doc_doc5+kDocStart

                xor     a
                ld      (MainDoc.Flags),a
                ld      hl,$BFFF
                ld      (MainDoc.Length),hl     ; test "full memory" case
                __T_SETUP '2'
                ld      hl,__t_doc_doc5+kDocStart
                ld      de,__t_doc_tk2
                call    docInsertLine
                call    pageVideo
                __T_MEMCMP __t_doc_doc5, <"ODS",0,-1,0, $12,OP_DAA,0, $12,OP_DAA,0, 0,-1>
                __T_BYTECMP MainDoc.Flags, 0
                __T_WORDCMP MainDoc.Length, $BFFF
                ld      a,__t_default_a
                __T_CHECK cf_de_hl : dw __t_doc_tk2, __t_doc_doc5+kDocStart

                ret

__t_doc_setupDoc:
                ld      hl,MainDoc.Pages
                ld      b,5
.l0:
                ldi     (hl),a          ; set five pages based on the argument in A (consecutive pages)
                inc     a
                djnz    .l0
                ld      (hl),$$Start    ; sixth page is the test-runner to keep it in memory
                ret

__t_doc_setupSet1:
                ld      a,$$__t_doc_doc1
                jr      __t_doc_setupDoc

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests
__t_doc_tk1:    db      $19, OP_RET
.l:             equ     $-__t_doc_tk1
__t_doc_tk2:    db      $12, OP_DAA
.l:             equ     $-__t_doc_tk2
                db      0

                display "[TEST RUN] tests: document.s                   Space: ",/A,$-__test_document_s," (+page 64)"

;;----------------------------------------------------------------------------------------------------------------------
; set fake document data into pages 64+
__t_doc_testr_old_dollar:

                MMU     0 4, 64, $0000
__t_doc_doc1:   db      "ODS", 0, -1,0, 0, -1               ; empty with single line
.e:
                ds      2, $CC      ; padding for docInsertSpace test
__t_doc_doc2:   db      "ODS", 0, -1,0, 0, 0, -1            ; two empty lines
__t_doc_doc3:   db      "ODS", 0, -1,0, $0B, ";", 0, 0, -1  ; indented ";" line + empty line
__t_doc_doc4:   db      "ODS", 0, -1,0, 0, -1               ; empty doc for docReplaceLine tests
.e:
                ds      10, $CC     ; padding for docReplaceLine
__t_doc_doc5:   db      "ODS", 0, -1,0, OP_RET, 0, OP_DAA, 0, OP_RRA, 0, -1
.e:

;;----------------------------------------------------------------------------------------------------------------------
; restore mapping and "$" to default + where test code did end (to continue with other tests)
                MMU     2 3, 10
                MMU     4 5, 4
                ORG     __t_doc_testr_old_dollar
