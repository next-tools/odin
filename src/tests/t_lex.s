;;----------------------------------------------------------------------------------------------------------------------
; lex.s tests
; coverage: writeLex, writeLexWord, lexSkipWS, lex

__test_lex_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Lex: "
                ; page in assembler module
                ld      a,(AsmCode)
                page    7,a

                ; TEST writeLex - internal API, but the only public entry point is the full "lex" call
                __T_SETUP 'w'
                __T_STRCPY AsmBuffer, "---\0"
                ld      hl,AsmBuffer
                ld      a,'a'
                call    writeLex
                __T_BYTECMP AsmBuffer, 'a'
                __T_CHECK ncf_a_hl : db 'a' : dw AsmBuffer+1
                __T_SETUP '.'       ; OOM error
                ld      hl,AsmBuffer+255
                ld      a,'!'
                call    writeLex
                ld      a,__t_default_a
                __T_BYTECMP AsmBuffer+255, '!'  ; but does still write the value
                __T_EXPECT_ERR
                __T_CHECK cf_hl : dw AsmBuffer

                ; TEST writeLexWord - internal API
                __T_SETUP 'W'
                __T_STRCPY AsmBuffer, "---\0"
                ld      hl,AsmBuffer
                ld      bc,'bc'
                call    writeLexWord
                __T_WORDCMP AsmBuffer, 'bc'
                __T_CHECK ncf_a_hl_bc : db 'b' : dw AsmBuffer+2, 'bc'   ; A==B is internal implementation detail only
                __T_SETUP '.'       ; OOM error on second write
                ld      hl,AsmBuffer+254
                ld      bc,'BC'
                call    writeLexWord
                ld      a,__t_default_a
                __T_WORDCMP AsmBuffer+254, 'BC' ; but does still write the value
                __T_EXPECT_ERR
                __T_CHECK cf_hl_bc : dw AsmBuffer, 'BC'
                __T_SETUP '.'       ; OOM error on first write
                ld      hl,AsmBuffer+255
                ld      bc,'XY'
                call    writeLexWord
                ld      a,__t_default_a
                __T_WORDCMP AsmBuffer+254, 'YC' ; does write bottom half of the value (to 255 offset)
                __T_BYTECMP AsmBuffer, 'c'      ; beginning of the buffer should keep 'c' from valid test
                __T_EXPECT_ERR
                __T_CHECK cf_hl_bc : dw AsmBuffer, 'XY'

                ; TEST lexSkipWS - internal API
                __T_SETUP 'S'
                ld      de,__t_lex_ws_t
                call    lexSkipWS
                __T_CHECK a_de : db 'a' : dw __t_lex_ws_t
                __T_SETUP '.'
                ld      de,__t_lex_ws_t+1
                call    lexSkipWS
                __T_CHECK a_de : db 'b' : dw __t_lex_ws_t+3
                __T_SETUP ':'
                ld      de,__t_lex_ws_t+3
                call    lexSkipWS
                __T_CHECK a_de : db 'b' : dw __t_lex_ws_t+3
                __T_SETUP '.'
                ld      de,__t_lex_ws_t+4
                call    lexSkipWS
                __T_CHECK a_de : db 'c' : dw __t_lex_ws_t+5
                __T_SETUP ':'
                ld      de,__t_lex_ws_t+6
                call    lexSkipWS
                __T_CHECK a_de : db 0 : dw __t_lex_ws_t+27

                ; TEST lex - public API - "empty" lines
                __T_SETUP 'l'
                ld      de,__t_lex_inp1     ; empty line
                call    lex
                __T_BYTECMP AsmBuffer, 0
                __T_CHECK only_ncf_de_hl : dw __t_lex_inp1+1, AsmBuffer
                __T_SETUP '.'
                ld      de,__t_lex_inp2     ; empty line with leading space
                ld      (AsmBuffer),hl      ; dirt into output buffer
                call    lex
                __T_BYTECMP AsmBuffer, 0
                __T_CHECK only_ncf_de_hl : dw __t_lex_inp2+2, AsmBuffer
                __T_SETUP ':'
                ld      de,__t_lex_inp3     ; empty line with eol comment
                ld      (AsmBuffer),hl      ; dirt into output buffer
                call    lex
                __T_BYTECMP AsmBuffer, 0
                __T_CHECK only_ncf_de_hl : dw __t_lex_inp3+6, AsmBuffer
                ; TEST lex - decimal values
                __T_SETUP '1'
                ld      de,__t_lex_inp4
                call    lex
                __T_MEMCMP AsmBuffer, <T_VALUE, 1, 0, 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inp4+2, AsmBuffer
                __T_SETUP '.'
                ld      de,__t_lex_inp5
                call    lex
                __T_MEMCMP AsmBuffer, <T_VALUE, low 1234, high 1234, T_VALUE, low 99999, high 99999, 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inp5+12, AsmBuffer
                ; TEST lex - hexa values and "current PC" $
                __T_SETUP '$'
                ld      de,__t_lex_inp6
                call    lex
                __T_MEMCMP AsmBuffer, <T_VALUE, $F, 0, T_VALUE, low $A1b2C, high $A1b2C, T_DOLLAR, 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inp6+13, AsmBuffer
                ; TEST lex - binary values
                __T_SETUP '%'
                ld      de,__t_lex_inp7
                call    lex
                __T_MEMCMP AsmBuffer, <T_VALUE, 0, 0, T_VALUE, low $ABCD, high $ABCD, ',', T_VALUE, 2, 0, 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inp7+33, AsmBuffer
                __T_SETUP '.'
                ld      de,__t_lex_inpI
                call    lex
                __T_EXPECT_ERR
                __T_CHECK cf                ; registers undefined
                __T_SETUP ':'
                ld      de,__t_lex_inpJ
                call    lex
                __T_EXPECT_ERR
                __T_CHECK cf                ; registers undefined
                ; TEST lex - tokens and alias-tokens
                __T_SETUP 't'
                ld      de,__t_lex_inp8
                call    lex
                __T_MEMCMP AsmBuffer, <OP_DEFB, OP_DEFB, OP_DEFB, T_VALUE, 9, 0, OP_DEFB, 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inp8+7, AsmBuffer
                __T_SETUP '.'
                ld      de,__t_lex_inp9
                call    lex
                __T_MEMCMP AsmBuffer, <OP_DEFB, T_VALUE, 9, 0, OP_DEFB, 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inp9+6, AsmBuffer
                ; TEST lex - symbols
                __T_SETUP 's'
                ld      de,__t_lex_inpA
                call    lex
                __T_MEMCMP AsmBuffer, <T_SYMBOL, ".\0", 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpA+2, AsmBuffer
                __T_SETUP '.'
                ld      de,__t_lex_inpB
                call    lex
                __T_MEMCMP AsmBuffer, <T_SYMBOL, "n\0", 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpB+3, AsmBuffer
                __T_SETUP ':'
                ld      de,__t_lex_inpB2
                call    lex
                __T_MEMCMP AsmBuffer, <T_SYMBOL, "m\0", ':', 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpB2+4, AsmBuffer
                __T_SETUP '.'
                ld      de,__t_lex_inpC
                call    lex
                __T_MEMCMP AsmBuffer, <T_SYMBOL, "_ab\0", '!', 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpC+5, AsmBuffer
                ; TEST lex - strings
                __T_SETUP '"'
                ld      de,__t_lex_inpD
                call    lex
                __T_MEMCMP AsmBuffer, <T_STRING, "a   b\0", T_SYMBOL, "a\0", T_SYMBOL, "b\0", 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpD+9, AsmBuffer
                __T_SETUP '.'
                ld      de,__t_lex_inpE
                call    lex
                __T_EXPECT_ERR
                __T_CHECK cf                ; registers undefined
                __T_SETUP ':'
                ld      de,__t_lex_inpF
                call    lex
                __T_MEMCMP AsmBuffer, <T_STRING, 0, 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpF+3, AsmBuffer
                __T_SETUP '.'
                ld      de,__t_lex_inpG     ; invalid value 9 inside string
                call    lex
                __T_CHECK cf    ; there's no error message for this case, just general CF=1 fail (should never happen)
                __T_SETUP ':'
                ld      de,__t_lex_inpH
                call    lex
                __T_MEMCMP AsmBuffer, <T_STRING, "a    b\0", 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpH+7, AsmBuffer
                ; TEST lex - other characters (partially covered by previous tests too, but here are extra cases)
                __T_SETUP '@'
                ld      de,__t_lex_inpK
                call    lex
                __T_EXPECT_ERR
                __T_CHECK cf                ; registers undefined
                __T_SETUP '.'
                ld      de,__t_lex_inpL
                call    lex
                __T_MEMCMP AsmBuffer, <'~','!','@','#','^','&','*','(',')','-','=','+','[',']','{','}','\','|',':',',','<','!>','?','/',0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpL+25, AsmBuffer        ; ^^^ '!>' exclamation is escaping > in macro argument
                ; TEST lex - strings in sinle-quote and mismatched quotes
                __T_SETUP "'"
                ld      de,__t_lex_inpM
                call    lex
                __T_MEMCMP AsmBuffer, <T_STRING, 0, 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpM+3, AsmBuffer
                __T_SETUP '.'
                ld      de,__t_lex_inpN
                call    lex
                __T_MEMCMP AsmBuffer, <T_STRING, "a    b\0", 0>
                __T_CHECK only_ncf_de_hl : dw __t_lex_inpN+7, AsmBuffer
                __T_SETUP ':'
                ld      de,__t_lex_inpO1
                call    lex
                __T_EXPECT_ERR
                __T_CHECK cf                ; registers undefined
                __T_SETUP '.'
                ld      de,__t_lex_inpO2
                call    lex
                __T_EXPECT_ERR
                __T_CHECK cf                ; registers undefined
                __T_SETUP ':'
                ld      de,__t_lex_inpO3
                call    lex
                __T_EXPECT_ERR
                __T_CHECK cf                ; registers undefined

                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

__t_lex_ws_t:   db      'a',$0a,$41,'b',$0b,'c',$0c,$0d,$0e,$0f,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$1a,$1b,$1c,$1d,$1e,$1f,$20,0

__t_lex_inp1:   db      0       ; empty line
__t_lex_inp2:   dz      $1a     ; empty line with leading space
__t_lex_inp3:   dz      $1a,";abc"          ; space + comment -> still empty line
__t_lex_inp4:   dz      "1"     ; value 1
__t_lex_inp5:   dz      "1234 99999;"       ; values 1234 and 99999 (16b overflow)
__t_lex_inp6:   dz      "$F $A1b2C $;"      ; hexa numbers and standalone T_DOLLAR
__t_lex_inp7:   dz      "%0 %1010_1011_1100_1101,%1__0___"  ; binary numbers
__t_lex_inp8:   db      OP_DEFB, OP_DEFB, 1, OP_DEFB, '9', OP_DEFB, 0   ; tokens and alias-tokens
__t_lex_inp9:   db      OP_DEFB, 1, '9', OP_DEFB, 1, 0
__t_lex_inpA:   dz      "."     ; symbol "dot only" - probably will become invalid after local labels implementation
__t_lex_inpB:   dz      "n:"    ; colon right after symbol is silently parsed with it
__t_lex_inpB2:  dz      "m :"   ; colon after space is left as standalone char
__t_lex_inpC:   dz      "_ab!"  ; symbol ending with non-id char
__t_lex_inpD:   db      '"a',$1e,'b"a',$1e,'b',0    ; string with three compressed spaces inside, terminated by "symbol"
__t_lex_inpE:   db      '"ab',0             ; string with missing terminating quote
__t_lex_inpF:   dz      '""'                ; empty string
__t_lex_inpG:   db      '"a',9,'"',0        ; invalid token inside string (byte value 9)
__t_lex_inpH:   db      '"a',$0a,4,'b"',0   ; $0a type of space compression (four spaces)
__t_lex_inpI:   dz      "%2"    ; bender's nightmare
__t_lex_inpJ:   dz      "%12"   ; bender's nightmare at second digit
__t_lex_inpK:   db      OP_DEFB, 9, OP_DEFB, 0  ; invalid character 9 in input
__t_lex_inpL:   dz      "~!@#^&*()-=+[]{}\\|:,<>?/" ; all other characters which don't form symbol or numeric literal
__t_lex_inpM:   dz      "''"                ; empty string in single-quote
__t_lex_inpN:   db      "'a",$0a,4,"b'",0   ; single-quote, $0a type of space compression (four spaces)
__t_lex_inpO1:  dz      "'ab"               ; string with missing terminating single-quote
__t_lex_inpO2:  dz      "'ab\""             ; string with mismatched terminating quote
__t_lex_inpO3:  dz      "\"ab'"             ; string with mismatched terminating quote

                display "[TEST RUN] tests: lex.s                        Space: ",/A,$-__test_lex_s
