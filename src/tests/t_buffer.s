;;----------------------------------------------------------------------------------------------------------------------
; buffer.s tests
; coverage: bufferClampPos, bufferDeleteChar, bufferInsertChar, bufferOverwriteChar
; missing: bufferFromDoc, bufferToDoc (a bit more pain to set up full fake document for those)

__test_buffer_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Buffer: "
                ; page in editor module
                ld      a,(EditorCode)
                page    7,a

                ; TEST bufferClampPos
                xor     a
                ld      (BufferLineLen),a
                __T_SETUP 'C'
                ld      a,1
                call    bufferClampPos
                __T_CHECK a : db 0
                ld      a,10
                ld      (BufferLineLen),a
                __T_SETUP '2'
                xor     a
                call    bufferClampPos
                __T_CHECK a : db 0
                __T_SETUP '.'
                ld      a,35
                call    bufferClampPos
                __T_CHECK a : db 10

                ; TEST bufferDeleteChar (short)
                ; set Editor buffer to "abc"
                ld      hl,__t_buf_buf1
                ld      de,EditorBuffer
                call    strcpy
                ld      a,__t_buf_buf1.l-1      ; buffer-index of the null-terminator
                ld      (BufferLineLen),a
                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP 'd'
                ld      l,3
                call    bufferDeleteChar
                __T_BYTECMP MainDoc.Flags, 0
                __T_CHECK zf_a_hl : db 3 : dw 3 | (__t_default_hl & $FF00)

                __T_SETUP '2'
                ld      l,1
                call    bufferDeleteChar
                __T_BYTECMP BufferLineLen, __t_buf_buf1.l-2
                __T_BYTECMP MainDoc.Flags, 1
                __T_MEMCMP EditorBuffer, "ac\0"
                __T_CHECK nzf_a_hl : db __t_buf_buf1.l-2 : dw 1 | (__t_default_hl & $FF00)

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'
                ld      l,1
                call    bufferDeleteChar
                __T_BYTECMP BufferLineLen, __t_buf_buf1.l-3
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP EditorBuffer, "\0a"
                __T_CHECK nzf_a_hl : db __t_buf_buf1.l-3 : dw 1 | (__t_default_hl & $FF00)

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'
                ld      l,1
                call    bufferDeleteChar
                __T_BYTECMP BufferLineLen, __t_buf_buf1.l-3
                __T_BYTECMP MainDoc.Flags, 0
                __T_WORDCMP EditorBuffer, "\0a"
                __T_CHECK zf_a_hl : db __t_buf_buf1.l-3 : dw 1 | (__t_default_hl & $FF00)

                ; TEST bufferInsertChar (short)
                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP 'i'
                ld      l,0
                ld      a,'q'
                call    bufferInsertChar
                __T_BYTECMP BufferLineLen, 2
                __T_BYTECMP MainDoc.Flags, 1
                __T_MEMCMP EditorBuffer, "qa\0"
                __T_CHECK ncf_a_hl : db 'q' : dw 0 | (__t_default_hl & $FF00)

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'
                ld      l,2
                ld      a,' '
                call    bufferInsertChar
                __T_BYTECMP BufferLineLen, 3
                __T_BYTECMP MainDoc.Flags, 1
                __T_MEMCMP EditorBuffer, "qa \0"
                __T_CHECK ncf_a_hl : db ' ' : dw 2 | (__t_default_hl & $FF00)

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'
                ld      l,1
                ld      a,'w'
                call    bufferInsertChar
                __T_BYTECMP BufferLineLen, 4
                __T_BYTECMP MainDoc.Flags, 1
                __T_MEMCMP EditorBuffer, "qwa \0"
                __T_CHECK ncf_a_hl : db 'w' : dw 1 | (__t_default_hl & $FF00)

                ; TEST bufferOverwriteChar (short)
                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP 'o'
                ld      l,1
                ld      a,'e'
                call    bufferOverwriteChar
                __T_BYTECMP BufferLineLen, 4
                __T_BYTECMP MainDoc.Flags, 1
                __T_MEMCMP EditorBuffer, "qea \0"
                __T_CHECK ncf_a_hl : db 'e' : dw 1 | (__t_default_hl & $FF00)

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'
                ld      l,4
                ld      a,'r'
                call    bufferOverwriteChar
                __T_BYTECMP BufferLineLen, 5
                __T_BYTECMP MainDoc.Flags, 1
                __T_MEMCMP EditorBuffer, "qea r\0"
                __T_CHECK ncf_a_hl : db 'r' : dw 4 | (__t_default_hl & $FF00)

                ; TEST bufferDeleteChar, bufferOverwriteChar, bufferInsertChar - long buffer (256)
                ld      hl,EditorBuffer
.fillBufL0:
                ld      a,l
                cpl
                ld      (hl),a
                inc     l
                jr      nz,.fillBufL0           ; EditorBuffer is $FF, $FE, ..., $02, $01, $00 (Full 255 chars)
                dec     a
                ld      (BufferLineLen),a       ; $FF - index of null-terminator
                xor     a
                ld      (MainDoc.Flags),a

                __T_SETUP 'D'                   ; try to delete from end (should fail)
                ld      l,$FF
                call    bufferDeleteChar
                __T_BYTECMP BufferLineLen, $FF
                __T_BYTECMP MainDoc.Flags, 0
                __T_WORDCMP EditorBuffer+254, $0001
                __T_CHECK zf_a_hl : db $FF : dw $FF | (__t_default_hl & $FF00)

                __T_SETUP '.'                   ; delete last char "$01"
                ld      l,$FE
                call    bufferDeleteChar
                __T_BYTECMP BufferLineLen, $FE
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP EditorBuffer+253, $0002
                __T_CHECK nzf_a_hl : db $FE : dw $FE | (__t_default_hl & $FF00)

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP 'O'                   ; overwrite one char in middle
                ld      l,127
                ld      a,'x'
                call    bufferOverwriteChar
                __T_BYTECMP BufferLineLen, $FE
                __T_BYTECMP MainDoc.Flags, 1
                __T_MEMCMP EditorBuffer+126, <~126, 'x', ~128>
                __T_CHECK ncf_a_hl : db 'x' : dw 127 | (__t_default_hl & $FF00)

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'                   ; overwrite at end (== insert) space
                ld      l,$FE
                ld      a,' '
                call    bufferOverwriteChar
                __T_BYTECMP BufferLineLen, $FF
                __T_BYTECMP MainDoc.Flags, 1
                __T_WORDCMP EditorBuffer+$FE, "\0 "
                __T_CHECK ncf_a_hl : db ' ' : dw $FE | (__t_default_hl & $FF00)

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP 'I'                   ; insert near end (and the "space" at end gives up)
                ld      l,$FD
                ld      a,'z'
                call    bufferInsertChar
                __T_BYTECMP BufferLineLen, $FF
                __T_BYTECMP MainDoc.Flags, 1
                __T_MEMCMP EditorBuffer+$FD, <'z', $02, $00>
                __T_CHECK ncf_a_hl : db 'z' : dw $FD | (__t_default_hl & $FF00)

                xor     a
                ld      (MainDoc.Flags),a
                __T_SETUP '.'                   ; insert near end - should fail this time
                ld      l,$FD
                ld      a,'n'
                call    bufferInsertChar
                __T_BYTECMP BufferLineLen, $FF
                __T_BYTECMP MainDoc.Flags, 0
                __T_MEMCMP EditorBuffer+$FD, <'z', $02, $00>
                __T_CHECK cf_a_hl : db 'n' : dw $FD | (__t_default_hl & $FF00)

                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

__t_buf_buf1:   dz      "abc"
.l:             equ     $-__t_buf_buf1

                display "[TEST RUN] tests: buffer.s                     Space: ",/A,$-__test_buffer_s
