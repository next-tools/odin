;;----------------------------------------------------------------------------------------------------------------------
;; Editor's command table
;;----------------------------------------------------------------------------------------------------------------------

;; This table maps keycodes to functions

CmdTable00:
                ; 00
                dw      cmdDoNothing            ; 
                dw      editorCmdExit           ; EDIT
                dw      cmdDoNothing            ; CAPS LOCK
                dw      editorCmdTab            ; TRUE VIDEO
                dw      editorScrollUp          ; INV VIDEO
                dw      editorCmdLeft           ; LEFT
                dw      editorCmdDown           ; DOWN
                dw      editorCmdUp             ; UP
                dw      editorCmdRight          ; RIGHT
                dw      editorScrollDown        ; GRAPH
                dw      editorCmdBackspace      ; DELETE
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      editorCmdNewLine        ; ENTER
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; 10
                dw      editorCmdPrevWord       ; SYM+W
                dw      editorCmdNextWord       ; SYM+E
                dw      cmdDoNothing            ; SYM+I
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      editorCmdCancel         ; BREAK
                dw      cmdDoNothing            ; SYM+SPACE
                dw      editorCmdFormatNewLine  ; SHIFT+ENTER
                dw      cmdDoNothing            ; SYM+ENTER
                dw      cmdDoNothing            ; 

CmdTable80:
                ; 80
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      editorCmdNewLineAbove   ; EXT+ENTER
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; 90
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; A0
                dw      editorCmdBlockMode      ; EXT+SPACE
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; B0
                dw      editorCmdDelete         ; EXT+0 (DELETE)
                dw      editorCmdSwitch         ; EXT+1 (EDIT)
                dw      cmdDoNothing            ; EXT+2 (CAPS LOCK)
                dw      editorCmdStartDoc       ; EXT+3 (TRUE VIDEO)
                dw      editorCmdEndDoc         ; EXT+4 (INV VIDEO)
                dw      editorCmdHome           ; EXT+5 (LEFT)
                dw      editorCmdPageDown       ; EXT+6 (DOWN)
                dw      editorCmdPageUp         ; EXT+7 (UP)
                dw      editorCmdEnd            ; EXT+8 (RIGHT)
                dw      cmdDoNothing            ; EXT+9 (GRAPH)
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; C0
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; D0
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; E0
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; EXT+A
                dw      cmdDoNothing            ; EXT+B
                dw      editorCmdCopy           ; EXT+C
                dw      editorCmdDupLine        ; EXT+D
                dw      editorCmdDeleteToEnd    ; EXT+E
                dw      cmdDoNothing            ; EXT+F
                dw      cmdDoNothing            ; EXT+G
                dw      cmdDoNothing            ; EXT+H
                dw      cmdDoNothing            ; EXT+I
                dw      cmdDoNothing            ; EXT+J
                dw      cmdDoNothing            ; EXT+K
                dw      cmdDoNothing            ; EXT+L
                dw      cmdDoNothing            ; EXT+M
                dw      cmdDoNothing            ; EXT+N
                dw      printChar.overwrite     ; EXT+O

                ; F0
                dw      cmdDoNothing            ; EXT+P
                dw      editorCmdFormat         ; EXT+Q
                dw      cmdDoNothing            ; EXT+R
                dw      editorCmdSave           ; EXT+S
                dw      cmdDoNothing            ; EXT+T
                dw      cmdDoNothing            ; EXT+U
                dw      editorCmdPaste          ; EXT+V
                dw      editorCmdClose          ; EXT+W
                dw      editorCmdCut            ; EXT+X
                dw      cmdDoNothing            ; EXT+Y
                dw      cmdDoNothing            ; EXT+Z
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
