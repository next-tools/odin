;;----------------------------------------------------------------------------------------------------------------------
;; OPTIONS
;;----------------------------------------------------------------------------------------------------------------------

;; Option table

OptTable:
                dz      "PAUSE"
                dw      optPause

                db      0

;;----------------------------------------------------------------------------------------------------------------------
;; asmOPT
;; Handles the OPT command
;;
;; Input:
;;      DE = lexical line input (256-byte aligned)
;;

asmOPT:
                ld      a,(Pass)
                and     a
                jp      nz,asmSkipLine  ; On pass > 0, we don't process again

                ld      a,(de)
                cp      T_SYMBOL
                jr      z,.no_error
.invalid:
                call    errorPrint
                dz      "INVALID OPT COMMAND"
                ret

.no_error:
                ld      hl,OptTable
                inc     e
.next_word:
                ; HL = Current word in opt table
                ; DE = input
                ld      a,(hl)
                and     a
                jr      z,.invalid      ; End of opt table
                push    de              ; Preserve beginning of word for next matching
.next_char:
                ld      a,(de)          ; Get next character in input
                inc     e
                call    upperCase
                cp      (hl)
                jr      nz,.mismatch
                inc     hl
                and     a
                jr      nz,.next_char   ; More characters to match

                ; Match found
                inc     sp
                inc     sp              ; Drop DE in stack
                ldhl
                jp      (hl)

                ; We've found mismatch
.mismatch:
                call    strEnd          ; advance OpTable to null terminator
                inc     hl
                inc     hl
                inc     hl              ; Skip null terminator and address
                pop     de
                jr      .next_word

;;----------------------------------------------------------------------------------------------------------------------
;; optTestEnd
;; Test that there are no parameters
;;
;; Output:
;;      CF = 1 if error
;;

optTestEnd:
                ld      a,(de)
                and     a
                ret     z
                call    errorPrint
                dz      "NO PARAMS EXPECTED AFTER OPT COMMAND"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; optPause
;; Set the debugger to wait for key before exiting
;;
;; Input:
;;      DE = lex input after label
;;

optPause:
                call    optTestEnd
                ret     c
                call    pageDebugger
                ld      a,1
                ld      (DebuggerPause),a
                ret

