;;----------------------------------------------------------------------------------------------------------------------
;; Handle for the LD instruction.  It's huge!  So requires its own source file
;;----------------------------------------------------------------------------------------------------------------------

R8_B            equ     $00     ; B
R8_C            equ     $01     ; C
R8_D            equ     $02     ; D
R8_E            equ     $03     ; E
R8_H            equ     $04     ; H
R8_L            equ     $05     ; L
R8_HL           equ     $06     ; (HL)
R8_A            equ     $07     ; A
R8_IXH          equ     $08     ; IXH
R8_IXL          equ     $09     ; IXL
R8_IYH          equ     $0a     ; IYH
R8_IYL          equ     $0b     ; IYL
R8_IX           equ     $0c     ; (IX)
R8_IY           equ     $0d     ; (IY)
R8_P_NNNN       equ     $0e     ; (nnnn)
R8_NN           equ     $0f     ; nn
R8_BC           equ     $10     ; (BC)
R8_DE           equ     $11     ; (DE)
R8_I            equ     $12     ; I
R8_R            equ     $13     ; R

R16_BC          equ     $00     ; BC
R16_DE          equ     $01     ; DE
R16_HL          equ     $02     ; HL
R16_SP          equ     $03     ; SP
R16_IX          equ     $04     ; IX
R16_IY          equ     $05     ; IY
R16_P_NNNN      equ     $06     ; (nnnn)
R16_NNNN        equ     $07     ; nnnn


;;----------------------------------------------------------------------------------------------------------------------
;; Recap:
;;
;; For all handlers we have these registers:
;;
;;      B = base opcode (this is irrelevant for LD)
;;      DE = address of first token after LD opcode
;;

asmLD:
                ; A = first token
                ; DE = address of next token

;;----------------------------------------------------------------------------------------------------------------------
;; First possible tokens after LD:
;;
;;      rr = BC, DE, HL, SP
;;      r = B, C, D, E, H, L, (HL), A
;;      nn = 16-bit value
;;      n = 8-bit value
;;
;;      SP                      LD SP,HL/IX/IY          (DD/FD) F9
;;
;;      BC, DE, HL, SP          LD rr,nn                (DD/FD) 00RR0001 nn nn
;;                              LD rr,(nn)              ED 01RR1011 nn nn
;;                                                      (DD/FD) 2A nn nn (for HL)
;;
;;      (                       LD (BC),A               02
;;                              LD (DE),A               12
;;                              LD (nn),HL              (DD/FD) 22 nn nn
;;                              LD (nn),A               32
;;                              LD (nn),rr              ED 01RR0011 nn nn
;;
;;      A                       LD A,(BC)               0A
;;                              LD A,(DE)               1A
;;                              LD A,(nn)               3A
;;                              LD A,I                  ED 57
;;                              LD A,R                  ED 5F
;;
;;      B, C, D, E, H, L, A,    LD r,r                  (DD/FD) 010RRR0RRR (nn)
;;      (HL), (IX+n), (IY+n)    LD r,n                  (DD/FD) 000RRR0110 (nn)
;;
;;      I                       LD I,A                  ED 47
;;
;;      R                       LD R,A                  ED 4F
;;

                call    ldGet8
                jr      c,.ld16bit              ; We expect a 16-bit load

                ld      b,c
                call    asmAcceptComma
                ret     c

                ; We look at the special case of LD (nnnn),A.  If we match (nnnn) but
                ; don't get an ,A following, then (nnnn) is a 16-bit source.
                ld      a,b
                cp      R8_P_NNNN
                jr      nz,.ld8bit              ; No? Definitely an 8-bit load.

                ld      c,OP_A
                call    asmCheck
                ld      b,R16_P_NNNN
                jr      c,.ld16bit_cont         ; try as 16-bit version of LD (nnnn)

                ; emit manually: ld (nnnn),A
                ld      a,$32
                call    emitByte
                jp      emitWord

                ; Handle 16-bit LD opcodes
.ld16bit:
                call    ldGet16                 ; C = operand index
                jp      c,invalidInst           ; Not a 16-bit load token, error!
                ld      b,c
                call    asmAcceptComma
                ret     c

.ld16bit_cont:
                call    ldGet16_nn
                ret     c

                ld      a,2*8
                ld      ix,LDTable16
                push    bc
                call    .write
                pop     bc
                ret     c

                ; Test to see if we write a 16-bit address/value
                ld      a,R16_P_NNNN-1
                cp      b
                jp      c,emitWord
                cp      c
                jp      c,emitWord
                ret

.ld8bit:
                ; HL = address or displacement
                ; Handle 8-bit LD opcodes
                ld      a,l
                ld      (.disp1_smc),a

                ld      a,(de)
                call    ldGet8_nn
                ret     c

                ; catch all combinations not covered by the big table and handle them with LDTable8_2 and LDTable8_3
                ld      a,R8_BC-1
                cp      b
                jr      c,.rare_arg1            ; arg1 is one of (BC), (DE), I, R
                cp      c
                jr      c,.rare_arg2            ; arg2 is one of (BC), (DE), I, R

                ; both arg1 and arg2 are from the big table range, use it
                ld      a,LDTable8.row_size
                ld      ix,LDTable8
                push    bc
                call    .write
                pop     bc
                ret     c

                ; Check to see if we need to write out an address or displacement for arg1
                ld      a,b
                cp      R8_IX
.disp1_smc+1:   ld      a,0
                call    nc,emitByte             ; arg1 is (ixy+?), emit displacement

                ; Check to see if we need to write out an address, value or displacement for arg2
                ld      b,l                     ; for emitB calls
                ld      a,R8_IX-1
                sub     c                       ; A=[-1,-2,-3,-4] for [R8_IX,R8_IY,R8_P_NNNN,R8_NN], A>=0 for R8_B..R8_A
                ret     nc                      ; arg2 is register or (hl) = done
                cp      R8_IX-1-R8_P_NNNN
                jp      z,emitWord              ; arg2 is (nnnn) = done
                jp      emitB                   ; arg2 is (ixy+?), emit displacement, or nn, emit value = done
                ASSERT R8_IX+1==R8_IY && R8_IY+1==R8_P_NNNN && R8_P_NNNN+1==R8_NN && R8_A<R8_IX

.rare_arg2:
                ; arg2 = (BC), (DE), I, R -> only LD A,arg2 is possible
                ld      ix,LDTable8_3 - 2*R8_A - 2*R8_BC
                ld      a,R8_A
                cp      b
                jr      .write_if_zf

.rare_arg1:
                ; arg1 = (BC), (DE), I, R -> only LD arg1,A is possible
                ld      ix,LDTable8_2 - 2*R8_BC - 2*R8_A
                ld      a,R8_A
                cp      c
.write_if_zf:
                scf
                ret     nz
                ld      a,2
                ;  |
                ; fallthrough into .write (and exit after writing opcode)
                ;  |
                ;  v

                ; Write 1 or 2 opcodes for the matching instruction
                ; A = width of row in bytes
                ; B = first arg index
                ; C = second arg index
                ; IX = table
.write:
                ; Look up the table
                push    de
                ld      d,b
                ld      e,a
                mul                             ; DE = row offset
                ld      a,c
                add     a,a
                add     de,a                    ; DE = location of 2-byte instruction bytes
                add     ix,de                   ; IX = opcode info
                pop     de

                ; Test first opcode (if 0 then instruction combination does not exist)
                ld      a,(ix+0)
                and     a
                scf
                ret     z                       ; If 0, invalid opcode
                call    emitByte

                ; Test second byte (if 0 then there is no 2nd byte)
                ld      a,(ix+1)
                and     a
                ret     z
                jp      emitByte

;;----------------------------------------------------------------------------------------------------------------------
;; ldGet8(_nn)
;; Convert the token to an index or set CF = 1 if error occurs
;;
;; Input:
;;      A = token
;;      DE = address of token
;;
;; Output:
;;      CF = 0 if match occurs
;;      C = index of 8-bit reference
;;      DE = points after tokens if matched
;;      DE is unchanged if CF = 1
;;      HL = value, address or displacement
;;

TEST_OP         macro   op
                inc     c
                cp      op
                ret     z
                endm

ldGet8_nn:
                call    exprIsNext
                jr      c,ldGet8

                ; Get nn
                ld      c,R8_NN
                call    exprGet
                ret     c

                ld      a,h
                and     a
                ret     z

                call    errorPrint
                dz      "8-BIT EXPRESSION EXPECTED"
                ret

ldGet8:
                inc     e
                cp      '('
                jr      z,.paren

                cp      OP_B
                ret     c               ; OP_B is first possible token, anything below is error

                ; check block of tokens OP_B to OP_A, excluding OP_AFA
                cp      OP_A+1
                jr      nc,.not_B_to_A
                cp      OP_AFA
                jr      z,.error
                sub     OP_B-R8_B       ; clears also CF
                ld      c,a             ; C = R8_B..R8_A
                ret
                ASSERT ($80==OP_B) && (0==R8_B) && (R8_C==OP_C-OP_B) && (R8_D==OP_D-OP_B) && (R8_E==OP_E-OP_B)
                ASSERT (R8_H==OP_H-OP_B) && (R8_L==OP_L-OP_B) && (R8_A==OP_A-OP_B) && (7==OP_A-OP_B)

.not_B_to_A:
                ; check block of tokens OP_IXH to OP_IYL
                cp      OP_IXH
                jr      c,.not_ixy8
                cp      OP_IYL+1
                jr      nc,.not_ixy8
                sub     OP_IXH-R8_IXH   ; clears also CF
                ld      c,a             ; C = R8_IXH..R8_IYL
                ret
                ASSERT (R8_IXH==OP_IXH-(OP_IXH-R8_IXH)) && (R8_IXL==OP_IXL-(OP_IXH-R8_IXH))
                ASSERT (R8_IYH==OP_IYH-(OP_IXH-R8_IXH)) && (R8_IYL==OP_IYL-(OP_IXH-R8_IXH)) && (3==OP_IYL-OP_IXH)

.not_ixy8:
                ld      c,R8_I
                cp      OP_I
                ret     z
                TEST_OP OP_R

.error:
                dec     e
                scf
                ret

.paren:
                push    de
                ld      a,(de)

                ld      c,R8_P_NNNN
                call    exprIsNext
                jr      c,.not_p_expr
                call    exprGet
                jr      nc,.good
.paren_error:
                pop     de
                jr      .error

.not_p_expr:
                inc     e
                ld      c,R8_HL
                cp      OP_HL
                jr      z,.good

                ld      c,R8_BC
                cp      OP_BC
                jr      z,.good

                inc     c
                cp      OP_DE
                jr      z,.good

                ld      c,R8_IX
                cp      OP_IX
                jr      z,.indexed
                inc     c
                cp      OP_IY
                jr      nz,.paren_error
.indexed:
                ld      hl,0
                ld      a,(de)
                cp      '+'
                jr      z,.calc
                cp      '-'
                jr      nz,.good
.calc:
                call    exprGet
                call    nc,asmValidDisp
                jr      c,.paren_error
.good:
                ld      a,(de)
                inc     e
                cp      ')'
                jr      nz,.paren_error
                inc     sp
                inc     sp
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; ldGet16(_nn)
;; Convert the token to an index or set CF = 1 if error occurs
;;
;; Input:
;;      A = token
;;      DE = address of token
;;
;; Output:
;;      C = index of 16-bit reference
;;      DE = points after tokens if matched
;;      ZF = 1 if match occurs
;;      DE is unchanged if ZF = 0
;;      HL = address or immediate value
;;

ldGet16_nn:
                ld      c,R16_NNNN
                call    exprIsNext
                jp      nc,exprGet

ldGet16:
                inc     e
                ld      c,R16_BC
                cp      OP_BC
                ret     z

                TEST_OP OP_DE
                TEST_OP OP_HL
                TEST_OP OP_SP
                TEST_OP OP_IX
                TEST_OP OP_IY

                inc     c
                cp      '('
                jr      nz,ldGet8.error

                ld      a,(de)
                push    de
                call    exprIsNext
                call    nc,exprGet
                jr      c,ldGet8.paren_error
                jr      ldGet8.good

;;----------------------------------------------------------------------------------------------------------------------
;; 8-bit table
;;
;; Defines the matrix of operations for 8-bit LD opcodes
;;
;; Indexes:
;;      $00 - B
;;      $01 - C
;;      $02 - D
;;      $03 - E
;;      $04 - H
;;      $05 - L
;;      $06 - (HL)
;;      $07 - A
;;      $08 - IXH
;;      $09 - IXL
;;      $0A - IYH
;;      $0B - IYL
;;      $0C - (IX)
;;      $0D - (IY)
;;      $0E - (nnnn)
;;      $0F - nn
;;      ; rare arguments handled by smaller tables
;;      $10 - (BC)
;;      $11 - (DE)
;;      $12 - I
;;      $13 - R
;;

LDTable8:
.row_size       equ     2*(R8_NN-R8_B+1)
                ; arg2: B     C     D     E     H     L     (HL)  A     IXH   IXL   IYH   IYL   (IX)  (IY)  (nnnn)nn        ; arg1:
                dw      $0040,$0041,$0042,$0043,$0044,$0045,$0046,$0047,$44dd,$45dd,$44fd,$45fd,$46dd,$46fd,$0000,$0006     ; B
                dw      $0048,$0049,$004a,$004b,$004c,$004d,$004e,$004f,$4cdd,$4ddd,$4cfd,$4dfd,$4edd,$4efd,$0000,$000e     ; C
                dw      $0050,$0051,$0052,$0053,$0054,$0055,$0056,$0057,$54dd,$55dd,$54fd,$55fd,$56dd,$56fd,$0000,$0016     ; D
                dw      $0058,$0059,$005a,$005b,$005c,$005d,$005e,$005f,$5cdd,$5ddd,$5cfd,$5dfd,$5edd,$5efd,$0000,$001e     ; E
                dw      $0060,$0061,$0062,$0063,$0064,$0065,$0066,$0067,$0000,$0000,$0000,$0000,$66dd,$66fd,$0000,$0026     ; H
                dw      $0068,$0069,$006a,$006b,$006c,$006d,$006e,$006f,$0000,$0000,$0000,$0000,$6edd,$6efd,$0000,$002e     ; L
                dw      $0070,$0071,$0072,$0073,$0074,$0075,$0000,$0077,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0036     ; (HL)
                dw      $0078,$0079,$007a,$007b,$007c,$007d,$007e,$007f,$7cdd,$7ddd,$7cfd,$7dfd,$7edd,$7efd,$003a,$003e     ; A
                dw      $60dd,$61dd,$62dd,$63dd,$0000,$0000,$0000,$67dd,$64dd,$65dd,$0000,$0000,$0000,$0000,$0000,$26dd     ; IXH
                dw      $68dd,$69dd,$6add,$6bdd,$0000,$0000,$0000,$6fdd,$6cdd,$6ddd,$0000,$0000,$0000,$0000,$0000,$2edd     ; IXL
                dw      $60fd,$61fd,$62fd,$63fd,$0000,$0000,$0000,$67fd,$0000,$0000,$64fd,$65fd,$0000,$0000,$0000,$26fd     ; IYH
                dw      $68fd,$69fd,$6afd,$6bfd,$0000,$0000,$0000,$6ffd,$0000,$0000,$6cfd,$6dfd,$0000,$0000,$0000,$2efd     ; IYL
                dw      $70dd,$71dd,$72dd,$73dd,$74dd,$75dd,$0000,$77dd,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$36dd     ; (IX)
                dw      $70fd,$71fd,$72fd,$73fd,$74fd,$75fd,$0000,$77fd,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$36fd     ; (IY)
                ; "(nnnn)," is emitted manually from code
                ; "nn," is impossible

LDTable8_2:     ; arg2: (BC)  (DE)  I     R
                dw      $0002,$0012,$47ed,$4fed     ; A,arg2

LDTable8_3:     ; arg1: (BC)  (DE)  I     R
                dw      $000a,$001a,$57ed,$5fed     ; arg1,A

;;----------------------------------------------------------------------------------------------------------------------
;; 16-bit table
;;
;; Defines the matrix of operations for 16-bit LD opcodes
;;
;;      $00 - BC
;;      $01 - DE
;;      $02 - HL
;;      $03 - SP
;;      $04 - IX
;;      $05 - IY
;;      $06 - (nnnn)
;;      $07 - nnnn
;;

LDTable16       ;       BC    DE    HL    SP    IX    IY    (nnnn)nnnn
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$4bed,$0001         ; BC
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$5bed,$0011         ; DE
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$002a,$0021         ; HL
                dw      $0000,$0000,$00f9,$0000,$f9dd,$f9fd,$7bed,$0031         ; SP
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$2add,$21dd         ; IX
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$2afd,$21fd         ; IY
                dw      $43ed,$53ed,$0022,$73ed,$22dd,$22fd,$0000,$0000         ; (nnnn)

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
