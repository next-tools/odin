#!/usr/bin/env bash

ASM=sjasmplus

$ASM src/main.s -I/env/src --zxnext=cspect --msg=war --fullpath || exit

echo Generating dot command...
cat odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data > odin
rm odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data

#echo Copying Odin to Emulator SD card...
hdfmonkey put $NEXT_ROOT/env/tbblue.mmc odin /dot
hdfmonkey put $NEXT_ROOT/env/tbblue.mmc docs/odin.gde /docs/guides
hdfmonkey put $NEXT_ROOT/env/tbblue.mmc etc/learn.odn /

#\env\hdfmonkey\hdfmonkey put \env\tbblue.mmc etc/plottest/* /
#call i.bat
#)
