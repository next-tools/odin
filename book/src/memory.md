# Memory Layout

This section describes the memory layout of both the code and the data generated
by Odin.

The code is organised as 8K page modules and are immediately loaded in the
highest page.  NextZXOS' allocator has a useful side-effect that when allocating
pages, it will always choose the highest page.  This is useful, because on a 2MB
machine, Odin and the data that is generated can stay out of the way of the
memory that could be used on a 1MB machine.  This means that if you decide to
write code for a 1MB machine, Odin will have little impact on the memory layout
of your program.

Data is generated as needed and this includes the final assembly code, source
code, symbol tables and other information.

Because of the unknown status of which pages are used by Odin and which are used
by the OS, the actual page number is stored in a 8-bit variable. The various
tables in this document that list any page allocations, along side it will be
the variable that holds the page number.

Odin will allocate pages for the following reasons:

* Code modules.
* Backing up video pages and system variables on start up.
* Storing documents.
* Storing filenames during assembly.
* Code pages for assembling into during normal assembly mode.
* Symbol tables.

## Page Layout

This table shows what gets paged into which slot:

| Slot | Address | Description of what gets paged here              |
|------|---------|--------------------------------------------------|
| 0    | $0000   | Debugger, document section 0                     |
| 1    | $2000   | Bootloader, document section 1                   |
| 2    | $4000   | Video data, system variables, document section 2 |
| 3    | $6000   | Document section 3                               |
| 4    | $8000   | Workspace, Filenames page, document section 4    |
| 5    | $a000   | Assembler filenames page, document section 5     |
| 6    | $c000   | Shared module, stack, assembly emission          |
| 7    | $e000   | Monitor, assembler and editor modules.           |

Slot 4 is used a lot for temporary paging. A call to `shared/page.s:readPage4`
returns the current page in slot 4. Usually a page is temporarily paged in there
(for example, saving documents, reading filenames), and then restored from the
value returned from `readPage4`.

## Bootloader and Paging

Odin is a dot file. Because of this fact, NextZXOS will switch in DivMMC RAM
into the address range of $2000-$3fff and load the first 8K of the dot file into
it. If the dot file is greater than 8K, the rest of the file is untouched.
However, NextZXOS will leave this file open and a call to the routine
`M_GETHANDLE` will return the file handle of the dot file to DE. This allows the
code to further read into the dot file.

Odin will use this feature to load the rest of its code modules, and then its
font data. You can see this at `main.s:Start`. Parameters are set up and
`main.s:installCode` is used to do the actual loading. Finally, a couple of
pages are allocated to back up the video and system variables state so they can
be restored on exit.

There are 6 modules that are loaded by the bootloader. Each with different
purpose, and eventually paged in at different page slots. Below is a table of
all the modules loaded:

| Name      | Page variable               | Page slot | Description                                          |
|-----------|-----------------------------|-----------|------------------------------------------------------|
| Shared    | `shared/page.s:SharedCode`  | 6         | Shared code available for all modules.               |
| Monitor   | `shared/page.s:MonitorCode` | 7         | Monitor module (`src/monitor`)                       |
| Editor    | `shared/page.s:EditorCode`  | 7         | Editor module (`src/editor`)                         |
| Assembler | `shared/page.s:AsmCode`     | 7         | Assembler module (`src/asm`)                         |
| Debugger  | `shared/page.s:DbgCode`     | 0         | Debugger module that runs user code (`src/debugger`) |
| Data      | `shared/page.s:Data`        | 1         | Font data                                            |

The Shared module is always present in page slot 6 ($c000-$dfff). This contains
code that manages files, keyboard, console display, documents and many code
utilities that are required for the other modules. `src/shared/page.s` has many
utilities for managing the page swapping to bring some of the 2MB into the 64K
address space that the CPU can see.

There are various `page` functions in this file that manage the various page
slot configuration modes necessary for Odin to work:

| Function      | Description                                                                                                 |
|---------------|-------------------------------------------------------------------------------------------------------------|
| pageDoc       | Pages the current document into slots 0-5.  Required before any reading or writing of the current document. |
| pageDivMMC    | Pages in the OS and video into slots 0-3 so that ROM routines are available.                                |
| pageOutDivMMC | Disables divMMC so that page slots 0-3 can be used for other reasons.                                       |
| pageVideo     | Pages in the video pages at slots 2-3 so that the display can be changed.                                   |
| pageDebugger  | Pages in the Odin debugger at slot 0.                                                                       |

Whenever Odin needs to access the document, `pageDoc` is called. Whenever,
rendering to the screen is required, `pageVideo` is called and so on.

The code NEVER assumes that a particular page configuration is present and will
declaratively set up the required page configuration without regard for previous
settings. This is very important for robust running of Odin as tracking down
paging bugs are difficult.

One of the most important functions for using modules is
`shared/page.s:farCall`. This will page in a module to slot 7 ($e000-$ffff),
jump to an address within it, then on return, it will restore the previous page
at slot 7 before continuing. This allows code from one module to call into
another and this is exactly what the monitor does when it needs to edit or
assemble.

When the modules are all loaded, the monitor module is paged in to slot 7, the
stack pointer is set to a buffer within slot 6 (the shared module), and
`monitor/monitor.s:monMain` is called. `monMain` only returns when the user hits
`Ext+Q` to quit. At this point, all the pages allocated are returned to the OS
and Odin exits.

There is an exception to this rule. If the assembler needs to enter `NEX` mode
to build the user's code, Odin is placed into a mode where the OS is ignored and
will not exit. Page allocation is handled differently by Odin and not done
through the OS. `NEX` mode is a special mode that allows users to generate NEX
files which are able to use any specific page in the Next's memory, even
accidently stomping Odin's pages. With Odin trying to allocate its pages from
the highest pages in RAM, if the user is trying to generate a NEX file for a 1MB
machine while running Odin on a 2MB machine, stomping should be avoided.

# Document Storage

All direct document access it handled by functions found in
`src/shared/document.s`. Odin can hold multiple documents in memory at once. The
maximum number of documents allowed are `shared/document.s:kNumDocs` plus one.
Each document is described by a structure called `shared/document.s:Document`.
The current document is stored at `shared/document.s:MainDoc` and the other
`kNumDocs` descriptions are stored at `shared/document.s:OtherDocs`.

When a different document is made current, it's info is swapped between an entry
in `OtherDocs` and `MainDoc`.

Odin allocates a single page to hold up to 32 256-byte filename strings. This
page is stored at `shared/document.s:FNamesPage`. The first entry is unused, the
following entries for the other documents leaving space for more temporary
filenames afterwards. The first non-document filename is indexed by
`shared/document.s:FNamesUserPage`. This is used by the assembler to store
filenames. Currently, Odin supports 17 documents in memory at any given time, so
there are 15 free entries left in the page. The filename for the current
document is always found at `shared/document.s:MainDocName`.

Each `Document` structure describes where the pages can be found using the
`Pages` field. This holds enough space for 7 page indices. However, a document
can only hold a maximum of 6 pages (i.e. 48K). The 7th element and any other
element that is not used (for documents smaller than 48K), is stored a dummy
page number. This allows any page to be paged into a slot, and the next page to
be paged into the next slot and the code does not need to worry about page
boundaries or paging in something past the 48K. To ensure that the dummy page
number is a valid one, we just use the same as the one stored in `FNamesPage` as
we know it will never be the same as a valid document page. Odin never actually
accesses non-document pages. This trick means we don't have to put in logic to
test for boundaries.

If a new document is created, all the info and filenames need to be shifted up
and the new one placed into `MainDoc` and `MainDocName`. Likewise, if a document
is deleted, the documents following it are shifted down.

## Document format

Each document has a 6 byte header, followed by its data, followed by the `$ff`
byte. `$ff` marks the boundaries of the document so Odin can quickly see if it's
overflowing or underflowing the document during processing. This table describes
the header.

| Offset | Length | Contents | Description                                                                                |
|--------|--------|----------|--------------------------------------------------------------------------------------------|
| 0      | 4      | 'ODS\0'  | Magic bytes.  The zero byte represents the format version.                                 |
| 4      | 1      | $ff      | Marks the beginning of the document.                                                       |
| 5      | 1      | $00      | Ensures that there is always an EOL byte ($00) before every line, including the first one. |

An empty document will have a single empty line. All documents must have an
empty line. This is represented by a single `$00` byte followed by the EOF byte
`$ff`.

So, in essence a document consists of the 6-byte header, any number of lines
terminated by `$00`, followed by a final `$ff`.

Each line is encoded with tokens to help compress the data within the 48K
document.  In this way, a document when expanded can be MUCH larger than 48K in
size.  The tokenisation allows for easy and fast parsing by the assembler.

Each byte has a specific meaning as described by this table:

| Bytes   | Description                                                                                                                                                                            |
|---------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| $00     | EOL character.  All lines are terminated with this byte.  This byte will not appear any where else.                                                                                    |
| $01-$03 | Aliased keyword (see below).                                                                                                                                                           |
| $04-$09 | Unused and will never appear in the document.                                                                                                                                          |
| $0a-$20 | Space characters. `$20` represents a single space as normal, up to `$0a` representing 23 spaces.  This means that all indentation and spacing is effectively encoded in a single byte. |
| $20-$7f | Normal ASCII characters.                                                                                                                                                               |
| $80-$fe | Keyword tokens.                                                                                                                                                                        |
| $ff     | End of document.                                                                                                                                                                       |

The token bytes (`$80`-`$fe`) represent keywords and are mapped to two different
tables. The first time a token is seen on a line, it is mapped to the first
table, the second and later times, it is mapped to the second table.

This table lists all the tokens:

| Byte | First token   | Second and later tokens |
| ---- | ------------- | ----------------------- |
| $80  | ADC           | B                       |
| $81  | ADD           | C                       |
| $82  | AND           | D                       |
| $83  | BIT           | E                       |
| $84  | CALL          | H                       |
| $85  | CCF           | L                       |
| $86  | CP            | AF'                     |
| $87  | CPD           | A                       |
| $88  | CPDR          | BC                      |
| $89  | CPI           | DE                      |
| $8a  | CPIR          | HL                      |
| $8b  | CPL           | SP                      |
| $8c  | DAA           | AF                      |
| $8d  | DEC           | I                       |
| $8e  | DB, DEFB      | IX                      |
| $8f  | DZ, DEFZ      | IY                      |
| $90  | DS, DEFS      | M                       |
| $91  | DW, DEFW      | NC                      |
| $92  | DI            |                         |
| $93  | OPT           | NZ                      |
| $94  | DJNZ          | P                       |
| $95  | EI            | PE, V                   |
| $96  | ENT           | PO, NV                  |
| $97  | EQU           | R                       |
| $98  | EX            |                         |
| $99  | EXX           | Z                       |
| $9a  | HALT          | <<                      |
| $9b  | IM            | >>                      |
| $9c  | IN            | MOD                     |
| $9d  | INC           | XH, IXH                 |
| $9e  | IND           | XL, IXL                 |
| $9f  | INDR          | YH, IYH                 |
| $a0  | INI           | YL, IYL                 |
| $a1  | INIR          |                         |
| $a2  | JP            |                         |
| $a3  | JR            |                         |
| $a4  | LD            |                         |
| $a5  | LDD           |                         |
| $a6  | LDDR          |                         |
| $a7  | LDI           |                         |
| $a8  | LDIR          |                         |
| $a9  | NEG           |                         |
| $aa  | NOP           |                         |
| $ab  | OR            |                         |
| $ac  | ORG           |                         |
| $ad  | OTDR          |                         |
| $ae  | OTIR          |                         |
| $af  | OUT           |                         |
| $b0  | OUTD          |                         |
| $b1  | OUTI          |                         |
| $b2  | POP           |                         |
| $b3  | PUSH          |                         |
| $b4  | RES           |                         |
| $b5  | RET           |                         |
| $b6  | RETI          |                         |
| $b7  | RETN          |                         |
| $b8  | RL            |                         |
| $b9  | RLA           |                         |
| $ba  | RLC           |                         |
| $bb  | RLCA          |                         |
| $bc  | RLD           |                         |
| $bd  | RR            |                         |
| $be  | RRA           |                         |
| $bf  | RRC           |                         |
| $c0  | RRCA          |                         |
| $c1  | RRD           |                         |
| $c2  | RST           |                         |
| $c3  | SBC           |                         |
| $c4  | SCF           |                         |
| $c5  | SET           |                         |
| $c6  | SLA           |                         |
| $c7  | SRA           |                         |
| $c8  | SRL           |                         |
| $c9  | SUB           |                         |
| $ca  | XOR           |                         |
| $cb  | SL1           |                         |
| $cc  | SWAP, SWAPNIB |                         |
| $cd  | MIRR, MIRROR  |                         |
| $ce  | TEST          |                         |
| $cf  | BSLA          |                         |
| $d0  | BSRA          |                         |
| $d1  | BSRL          |                         |
| $d2  | BSRF          |                         |
| $d3  | BRLC          |                         |
| $d4  | MUL           |                         |
| $d5  | OTIB, OUTINB  |                         |
| $d6  | NREG, NEXTREG |                         |
| $d7  | PXDN, PIXELDN |                         |
| $d8  | PXAD, PIXELAD |                         |
| $d9  | STAE, SETAE   |                         |
| $da  | LDIX, LDIRX   |                         |
| $db  | LDWS          |                         |
| $dc  | LDDX          |                         |
| $dd  | LIRX          |                         |
| $de  | LPRX, LDPIRX  |                         |
| $df  | LDRX, LDDRX   |                         |
| $e0  | BIN, INCBIN   |                         |
| $e1  | LOAD, INCLUDE |                         |
| $e2  | SAVE          |                         |
| $e3  | DC, DEFC      |                         |
| $e4  | EXA           |                         |
| $e5  | TAB           |                         |
| $e6  | ENDT          |                         |
| $e7  | ENTB          |                         |
| $e8  | ENTC          |                         |
| $e9  | ENTS          |                         |
| $ea  | ENTW          |                         |
| $eb  | ENTZ          |                         |

You may notice in the table that there are several keywords for the same byte.
This is because different keywords can be aliases of each other. For example,
`DEFB` and `DB` mean the same thing. To make sure when editing the document you
see the original version, any alias (i.e. 2nd entry in the column) is prefixed
with the bytes `$01` to `$03`, where `$01` means 2nd version, and `$03` means
4th version. The first version has no prefixes.

The Z80N CPU has new opcodes that are larger than 4 characters. Some people like
their opcode field to line up and to save screen estate there are 4 character
aliases of them. For example, `NEXTREG` has the alias `NREG`. Currently, these
aliases are only standard for Odin. No other assembler supports these versions.

# Assembly

The first thing the assembler does is allocate a page to store filenames. The
assembler keeps a context stack of files it is assembling and it stores their
filenames in this page and stores it in `asm/asm.s:AsmFNamePage`. This context
stack is stored at `asm/asm.s:AsmStack` that references the filenames within
this page.

The context also stores the document offset of the current line, the actual line
number, the pages used by the document being assembled (either references
documents in editor or temporary pages from loading from SD card), and a flag
determining whether it was loaded from a file or accessed from the editor.

The index register `IY` always points to the current context during assembly.

While assembling, the assembler keeps track of which 8 pages to assemble to.
These pages are stored at `shared/var.s:MMUState`. It is 9 pages long so 2 pages
can be paged in at all times so not to worry about crossing page boundaries. In
normal assembly mode, the assembler allocates 4 pages for `MMUState` slots 4-7.
This allows a 32K program to be assembled that does not require paging.

For more advanced paging options and to be able to assemble anywhere, `NEX` mode
must be entered via a `OPT NEX` directive in the source. As soon as that is
assembled, Odin enters Nex mode and does not play nice with the OS any longer.
Therefore, it cannot return to BASIC.

## Symbol table

Symbol information is generated by the assembler and kept even after assembly so
that the monitor and debugger can use them.

The symbol table manager allocates up to 8 pages to store them, allowing space
for up to 2048 symbols.  Each symbol takes up 32 bytes of memory, so each page
can hold 256.

The page indexes used by the symbol table are stored at `shared/var.s:SymPages`,
their count at `shared/var.s:NumSymPages` and the current new symbol pointer at
`shared/var.s:NumSyms`.  New pages are allocated as the previous ones are filled
up.

The format of the 32 bytes per symbol are described in this table:

| Offset | Length | Description                                                 |
|--------|--------|-------------------------------------------------------------|
| 0      | 1      | XOR of all the bytes in the symbol name to speed up search. |
| 1      | 26     | Symbol characters.  Null terminated.                        |
| 27     | 1      | Null terminator (in case symbol is 27 chars long).          |
| 28     | 1      | Flags.                                                      |
| 29     | 1      | Reserved.                                                   |
| 30     | 2      | 16-bit value (address or equ).                              |

## Local symbols

Local symbols are treated differently. They use any space in the symbol table
pages. They use a different format:

| Offset | Length | Description                                            |
|--------|--------|--------------------------------------------------------|
| 0      | 8      | Up to 8 characters of the local label.                 |
| 8      | 1      | Reserved byte.                                         |
| 9      | 2      | 16-bit value (address).                                |
| 11     | 20     | Up to 10 references to write the address after pass 2. |
| 31     | 1      | Number of references.                                  |

Local variables are different in that they can only be 8 characters long, and
there may only be up to 10 forward references to them. When another global
symbol is seen, the local symbols are removed from the table by resetting the
`NumSyms` counter.

# 256-byte buffers

Throughout the codebase there are various 256-byte buffers used for various
reasons. Modules might have their own so they do not have to rely on certain
pages being paged in. Below is a table of all the 256-byte buffers, their module
and labels and the description of what they're used for:

| Module  | Label                                     | Description                                                   | Aligned |
|---------|-------------------------------------------|---------------------------------------------------------------|---------|
| Shared  | KeyboardBuffer                            | Stores the keypresses from the keyboard interrupt routine.    | Yes     |
|         | TokenisedBuffer / ExprBuffer / FileBuffer | Used for loading/saving, tokenisation and expression parsing. | Yes     |
|         | Stack                                     | Contains the programs stack.                                  | Yes     |
|         | MainDocName                               | Stores the current document name                              | No      |
| Monitor | MonitorBuffer                             |                                                               | Yes     |
| Editor  | EditorBuffer                              | Used to store the current edited line.                        | Yes     |

The `Aligned` columns states whether the 256-byte buffer is aligned on a
256-byte page (i.e. the low byte of the address is `$00`). If it is aligned,
then tricks such as only incrementing the low-byte register (e.g. E in DE) can
be used. This is quicker and also allows for wrap-around if that is required.

Also worth mentioning is a 300-byte non-aligned buffer stored at
`shared/var.s:FileName` that is used to store filenames as input parameters to
various routines in Odin.
