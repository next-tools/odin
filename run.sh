source make.sh

if [ $? -eq 0 ]
then
  hdfmonkey put ../env/tbblue.mmc etc/autoexec.bas /nextzxos
  mono $NEXT_ROOT/env/cspect/CSpect.exe -sound -r -tv -brk -16bit -s28 -w3 -zxnext -nextrom -map=odin.map -mmc=$NEXT_ROOT/env/tbblue.mmc
fi

