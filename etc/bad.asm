; List of bad opcodes that should NOT assemble

NN              equ     $1234
DISP            equ     -128
N               equ     $56

                ; Can't access if IXH/IXL if (IX) is in opcode
                ld      ixh,(IX+DISP)
                ld      ixl,(IX+DISP)
                ld      (IX+DISP),ixh
                ld      (IX+DISP),ixl

                ; These patterns are replaced by HALT
                ld      (hl),(hl)
                ld      (ix+DISP),(hl)
                ld      (hl),(ix+DISP)
                ld      (ix+DISP),(ix+DISP)

