ODIN'S EDITOR HOTKEYS
~~~~~~~~~~~~~~~~~~~~~

EDIT            Switch back to monitor mode
TRUE VIDEO      Insert spaces until next tab field is reached
LEFT            Move cursor left
RIGHT           Move cursor right
UP              Move cursor up
DOWN            Move cursor down
DELETE          Delete character before cursor

EXTENDED KEYS
~~~~~~~~~~~~~

The following keys should be pressed while pressing EXTENDED MODE:

EDIT            Show document selection menu, when editing 2 or more docs
TRUE VIDEO      Move to start of document
INV VIDEO       Move to end of document
LEFT            Move to beginning of line
RIGHT           Move to end of line
UP              Page up
DOWN            Page down
DELETE          Delete character under cursor

C               Copy current line into clipboard
D               Duplicate current line below and move there
S               Save current document
V               Paste line from clipboard before current line
X               Delete current line and store into clipboard

